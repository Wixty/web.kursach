<?php

return [

    // app name
    'MusiKo Shop' => 'Магазин MusiKo',

    // nav menu
    'Home'      => 'Главная',
    'About us'  => 'О нас',
    'Contacts'  => 'Контакты',
    'Language'  => 'Язык',
    'Cart'      => 'Корзина',
    'Goods Catalog'     => 'Каталог Товаров',
    'All goods'         => 'Все товары',
    'All {type}'        => 'Все {type}',
    'News and Deals'    => 'Новости и Акции',

    // footer
    'Admin Panel'   => 'Админка',

    // homepage
    'Welcome to MusiKo shop!' => 'Добро пожаловать в магазин MusiKo',
    'Choose the Best!' => 'Выбирайте Лучшее!',

    // goods
    'Sorry, goods is not found.' => 'Извините, товары не найдены.',
    'Add to cart'   => 'Добавить в корзину',
    'Out of stock'  => 'Закончились',
    'In stock'  => 'В наличии',
    'Filter'    => 'Фильтр',
    'Reset'     => 'Сброс',
    'Name'      => 'Название',
    'Type'      => 'Тип',
    'Category'  => 'Категория',
    'Photo'     => 'Фото',
    'Price'     => 'Цена',
    'Count'     => 'Кол-во',

    // cart
    'Your cart is empty!'   => 'Ваша корзина пуста!',
    'Your cart contains'    => 'В вашей корзине',
    'This goods is no longer available.'        => 'Этот товар уже недоступен.',
    'This goods is no longer in stock, sorry.'  => 'Этот товар закончился.',
    'This goods was added to the cart successfully!' => 'Этот товар был успешно добавлен в корзину!',
    'To order goods, please fill in the following fields' => 'Чтобы заказать товары, пожалуйста, заполните следующие поля',
    'You already have max count of this goods in cart. If you need more, please wrote it in the comment field for order.'
        => 'В вашей корзине уже есть максимально разрешённое количество данного товара. Если вам нужно больше, пожалуйста, напишите это в комментарии к заказу.',
    'Something going wrong, please try again later.' => 'Что-то пошло не так. Пожалуйста, попробуйте ещё раз чуть позже.',

    // order form
    'Payment method'    => 'Способ оплаты',
    'Contact telephone' => 'Контактный телефон',
    'Full name'     => 'ФИО',
    'Full address'  => 'Полный адрес',
    'Email'     => 'Электронная почта',
    'Comment'   => 'Комментарий',
    'Bot Check' => 'Проверка на ботов',
    'Place an order' => 'Оформить заказ',
    'Total price of the order: ${sum}' => 'Общая сумма заказа: ${sum}',
    'Credit Card - {types}' => 'Кредитная карта',
    'Cash' => 'Наличные',
    'PayPal' => 'PayPal',
    'Qiwi' => 'Qiwi кошелёк',
    'Yandex.Money' => 'Яндекс.Деньги',
    'Place the order' => 'Оставить заказ',
    'Choose one...' => 'Выберите один...',

    // order response
    'The order was successfully placed. Our manager will contact you within an hour.'
        => 'Заказ успешно размещён. Наш менеджер свяжется с вами в течение часа.',
    'An error was occurred while creating the order. Please try again a later.'
        => 'При создании заказа произошла ошибка. Пожалуйста, попробуйте ещё раз чуть позже.',
    'There was an error creating the order. Check the correctness of filling the form.'
        => 'При создании заказа произошла ошибка. Проверьте корректность заполнения формы.',


];