<?php

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(__DIR__, 2) . '/frontend');
Yii::setAlias('@backend', dirname(__DIR__, 2) . '/backend');
Yii::setAlias('@console', dirname(__DIR__, 2) . '/console');

Yii::setAlias('@front_url', 'http://shop.wixty.ru');
Yii::setAlias('@back_url', 'http://admin.shop.wixty.ru');
Yii::setAlias('@front_web', dirname(__DIR__, 2) . '/public_html/shop');
Yii::setAlias('@back_web', dirname(__DIR__, 2) . '/public_html/admin');
