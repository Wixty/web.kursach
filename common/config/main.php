<?php

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'components' => [
        'cache' => [
            'class' => yii\caching\FileCache::class,
        ],
        'i18n' => [
            'translations' => [
                'msg' => [
                    'class' => \yii\i18n\PhpMessageSource::class,
                    'basePath' => '@common/translations',
                ]
            ]
        ],
    ],
];
