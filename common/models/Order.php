<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\{ ActiveQuery, ActiveRecord };


/**
 * Class Order
 * @package common\models
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $telephone
 * @property string $email
 * @property integer $pay_method
 * @property string $comment
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Cart[] $carts
 * @property Goods[] $goods
 * @property-read $payMethods
 * @property-read $payMethodsIDs
 * @property-read $statuses
 * @property-read $statusesIDs
 * @property-read integer $created
 * @property-read integer $updated
 */
class Order extends ActiveRecord
{
    public const
        PAY_CARD    = 0,
        PAY_CASH    = 1,
        PAY_PAYPAL  = 2,
        PAY_QIWI    = 3,
        PAY_YANDEX  = 4,
        STATUS_NEW       = 0,
        STATUS_IN_WORK   = 1,
        STATUS_COMPLETED = 2;

    public static function tableName()
    {
        return '{{%order}}';
    }

    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['comment', 'default', 'value' => ''],
            [['name', 'address', 'telephone', 'email'], 'string', 'min' => 7, 'max' => 255, 'skipOnEmpty' => false],
            ['pay_method', 'in', 'range' => self::getPayMethodsIDs(), 'skipOnEmpty' => false],
            ['status', 'in', 'range' => self::getStatusesIDs(), 'skipOnEmpty' => false],
            [['comment', 'ip'], 'string', 'min' => 0, 'max' => 255],
            ['email', 'email'],
        ];
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @return ActiveQuery|Cart[]
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::class, ['order_id' => 'id']);
    }

    /**
     * @return ActiveQuery|Goods[]
     */
    public function getGoods()
    {
        return $this->hasMany(Goods::class, ['id' => 'goods_id'])
            ->via('carts');
    }

    public static function getPayMethods() : array
    {
        return [
            self::PAY_CARD => Yii::t('msg', 'Credit Card - {types}', ['types' => 'Visa, MasterCard, Maestro, Mir']),
            self::PAY_CASH => Yii::t('msg', 'Cash'),
            self::PAY_PAYPAL => Yii::t('msg', 'PayPal'),
            self::PAY_QIWI => Yii::t('msg', 'Qiwi'),
            self::PAY_YANDEX => Yii::t('msg', 'Yandex.Money'),
        ];
    }

    public static function getPayMethodsIDs()
    {
        return array_keys(self::getPayMethods());
    }

    public static function getStatuses() : array
    {
        return [
            self::STATUS_NEW => 'New',
            self::STATUS_IN_WORK => 'In work',
            self::STATUS_COMPLETED => 'Completed',
        ];
    }

    public static function getStatusesIDs()
    {
        return array_keys(self::getStatuses());
    }

    public function getCreated()
    {
        return date('d/m/Y H:i:s', $this->created_at);
    }

    public function getUpdated()
    {
        return date('d/m/Y H:i:s', $this->updated_at);
    }

}