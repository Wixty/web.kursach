<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace common\models;

use Yii;
use yii\db\{ ActiveQuery, ActiveRecord };
use yii\helpers\ArrayHelper;


/**
 * Class Cart
 * @package common\models
 *
 * @property integer $order_id
 * @property integer $goods_id
 * @property integer $count
 *
 * @property Order $order
 * @property Goods $goods
 */
class Cart extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%cart}}';
    }

    public function rules()
    {
        return [
            [['order_id', 'goods_id', 'count'], 'integer', 'min' => 1, 'skipOnEmpty' => false],
            [['order_id', 'goods_id'], 'unique', 'targetAttribute' => ['order_id', 'goods_id']],
            ['order_id', 'exist', 'targetClass' => Order::class, 'targetAttribute' => 'id'],
            ['goods_id', 'exist', 'targetClass' => Goods::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @return ActiveQuery|Order
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    /**
     * @return ActiveQuery|Goods
     */
    public function getGoods()
    {
        return $this->hasOne(Goods::class, ['id' => 'goods_id']);
    }



    private static function get() : array
    {
        return Yii::$app->session->get('cart', []);
    }

    private static function set(array $cart = []) : void
    {
        Yii::$app->session->set('cart', $cart);
    }


    /**
     * Возвращает массив с двумя подмассивами: первый - модели товаров из корзины, второй - сама корзина.
     * Для массива корзины: ключ - id товара в корзине, значение - количество.
     * Дополнительно проверяет наличие всех товаров из корзины в необходимом количестве
     * и ограничение по максимальному количеству одного товара в корзине.
     * @return array
     */
    public static function checkAndGet() : array
    {
        $cart = self::get();
        $goods_in_cart = !empty($cart) ? Goods::findInStock()->andWhere(['id' => array_keys($cart)])->all() : [];

        $stock = ArrayHelper::map($goods_in_cart, 'id', 'in_stock');
        foreach ($cart as $id => $count) {
            // если товара вообще нет в наличии
            if (!isset($stock[$id])) unset($cart[$id]);
            // если в корзине больше, чем в наличии
            elseif ($count > $stock[$id]) $cart[$id] = $stock[$id];
            // если в корзине больше максимального разрешенного количества
            elseif ($count > Goods::MAX_COUNT_IN_CART) $cart[$id] = Goods::MAX_COUNT_IN_CART;
        }
        unset($stock);

        self::set($cart);
        return [$cart, $goods_in_cart];
    }

    /**
     * Добавляет в корзину товар с указанным id.
     * Дополнительно проверяет наличие товара в необходимом количестве
     * и ограничение по максимальному количеству одного товара в корзине.
     * Возвращает массив с двумя элементами: status - danger|warning|success, message - сообщение.
     * @param int $id
     * @return array
     */
    public static function add(int $id) : array
    {
        $goods = $id > 0 ? Goods::findOne($id) : null;

        if (!$goods) return [
            'status' => 'danger',
            'message' => Yii::t('msg', 'This goods is no longer available.'),
        ];

        $cart = self::get();
        if (isset($cart[$id])) {
            if ($cart[$id] >= Goods::MAX_COUNT_IN_CART)
                return [
                    'status' => 'warning',
                    'message' => Yii::t('msg', 'You already have max count of this goods in cart. If you need more, please wrote it in the comment field for order.')
                ];
            if (!$goods->isInStock || $goods->in_stock < $cart[$id]+1)
                return [
                    'status' => 'warning',
                    'message' => Yii::t('msg', 'This goods is no longer in stock, sorry.'),
                ];
        }
        $cart[$id] = ($cart[$id] ?? 0) + 1;
        self::set($cart);

        return [
            'status' => 'success',
            'message' => Yii::t('msg', 'This goods was added to the cart successfully!'),
        ];
    }

    /**
     * Удаляет товар с указанным id из корзины.
     * Возвращает массив с одним элементом: success - true|false.
     * @param int $id
     * @return array
     */
    public static function remove(int $id)
    {
        $cart = self::get();

        if (!isset($cart[$id]))
            return [ 'success' => false ];

        if ($cart[$id] === 1) unset($cart[$id]);
        else --$cart[$id];

        self::set($cart);
        return [ 'success' => true ];
    }

    /**
     * Сохраняет корзину в БД. Если
     * @param int $order_id
     * @return bool
     */
    public static function saveForOrder(int $order_id)
    {
        $cart = self::get();
        $goods_in_cart = Goods::findInStock()->andWhere(['id' => array_keys($cart)])->all();

        if(\count($cart) !== \count($goods_in_cart)) return false;

        $stock = ArrayHelper::map($goods_in_cart, 'id', 'in_stock');
        foreach ($cart as $goods_id => $count)
            if ($count > $stock[$goods_id]) return false;

        // если метод всё ещё выполняется, то можно сохранять
        foreach ($goods_in_cart as $goods) {
            $count = $cart[$goods->id];
            $goods->updateCounters(['in_stock' => - $count]);
            $cart_model = new self([
                'order_id' => $order_id,
                'goods_id' => $goods->id,
                'count' => $count,
            ]);
            if (!$cart_model->save()) // вообще говоря исключительная ситуация
                Yii::error(['$cart_model->save() error', $cart_model->errors]);
        }

        self::set();
        return true;
    }

}