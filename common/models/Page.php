<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class Page
 * @package common\models
 *
 * @property integer $id
 * @property string $url
 * @property string $content_en
 * @property string $content_ru
 *
 * @property-read string $title_en
 * @property-read string $title
 * @property-read string $content
 */
class Page extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%page}}';
    }

    public function rules()
    {
        return [
            [['!title_en', '!url'], 'string', 'min' => 1, 'max' => 255, 'skipOnEmpty' => false],
            [['content_en', 'content_ru'], 'string', 'skipOnEmpty' => false],
            ['url', 'match', 'pattern' => '/^[\w-]+$/'], // JS pattern!!! a-zA-Z0-9_-
            ['url', 'unique'],
        ];
    }

    public function getContent(): string
    {
        $attribute = 'content_'.Lang::getShort();
        return $this->$attribute;
    }

    public function getTitle(): string
    {
        return \Yii::t('msg', $this->title_en);
    }

}