<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace common\models;

use yii\db\{ ActiveQuery, ActiveRecord };
use yii\helpers\ArrayHelper;


/**
 * Class Category
 * @package common\models
 *
 * @property integer $id
 * @property string $name_en
 * @property string $name_ru
 * @property integer $type_id
 *
 * @property-read string $name
 *
 * @property Goods[] $goods
 * @property Type $type
 */
class Category extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%category}}';
    }

    public function rules()
    {
        return [
            [['name_en', 'name_ru'], 'string', 'min' => 1, 'max' => 255, 'skipOnEmpty' => false],
            ['type_id', 'exist', 'targetClass' => Type::class, 'targetAttribute' => 'id', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @return ActiveQuery|Goods[]
     */
    public function getGoods()
    {
        return $this->hasMany(Goods::class, ['category_id' => 'id']);
    }

    /**
     * @return ActiveQuery|Type
     */
    public function getType()
    {
        return $this->hasOne(Type::class, ['id' => 'type_id']);
    }

    public function getName()
    {
        $attribute = 'name_'.Lang::getShort();
        return $this->$attribute;
    }

    public static function getDropDownList() : array
    {
        $categories = static::find()->select(['id', 'name_'.Lang::getShort()])->all();
        return ArrayHelper::map($categories, 'id', 'name_'.Lang::getShort());
    }

}