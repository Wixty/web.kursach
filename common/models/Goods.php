<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace common\models;

use Yii;
use yii\db\{ ActiveQuery, ActiveRecord };
use Imagine\Image\ImageInterface;
use zxbodya\yii2\galleryManager\{ GalleryBehavior, GalleryImage };


/**
 * Class Goods
 * @package common\models
 *
 * @property integer $id
 * @property double $price
 * @property string $name_ru
 * @property string $name_en
 * @property string $description_en
 * @property string $description_ru
 * @property string $image
 * @property integer $in_stock
 * @property integer $category_id
 *
 * @property Category $category
 * @property Type $type
 *
 * @property-read string $name
 * @property-read string $description
 * @property-read GalleryImage[] $galleryImages
 * @property-read string $imageUrl
 * @property-read string $imageFullUrl
 * @property-read bool $isInStock
 */
class Goods extends ActiveRecord
{

    public const MAX_COUNT_IN_CART = 3;

    public $image_file;

    public static function tableName()
    {
        return '{{%goods}}';
    }

    public function rules()
    {
        return [
            [['name_en', 'name_ru', 'description_en', 'description_ru'], 'string', 'min' => 1, 'skipOnEmpty' => false],
            [['price'], 'double', 'min' => 0.01, 'skipOnEmpty' => false],
            [['in_stock'], 'integer', 'min' => 0, 'skipOnEmpty' => false],
            ['image', 'string', 'skipOnEmpty' => false],
            ['image_file', 'image', 'on' => 'edit', 'skipOnEmpty' => false],
            ['category_id', 'exist', 'targetClass' => Category::class, 'targetAttribute' => 'id'],
        ];
    }

    public function behaviors()
    {
        return [
            'gallery' => [
                'class' => GalleryBehavior::class,
                'type' => 'goods',
                'tableName' => '{{%goods_gallery}}',
                'timeHash' => false,
                'extension' => 'jpg',
                'directory' => Yii::getAlias('@front_web/images/goods-gallery'),
                'url' => Yii::getAlias('@front_url/images/goods-gallery'),
                'versions' => [
                    'medium' => function ($img) {
                        /** @var ImageInterface $img */
                        $dstSize = $img->getSize();
                        $maxWidth = 600;
                        if ($dstSize->getWidth() > $maxWidth) {
                            $dstSize = $dstSize->widen($maxWidth);
                        }
                        return $img->copy()->resize($dstSize);
                    },
                ],
            ]
        ];
    }

    /**
     * Возвращает все связанные объекты GalleryImage для текущего товара.
     * @return GalleryImage[]
     */
    public function getGalleryImages()
    {
        /* @var $gallery GalleryBehavior */
        $gallery = $this->getBehavior('gallery');
        return $gallery->getImages();
    }

    public function getName()
    {
        $attribute = 'name_'.Lang::getShort();
        return $this->$attribute;
    }

    public function getDescription()
    {
        $attribute = 'description_'.Lang::getShort();
        return $this->$attribute;
    }

    /**
     * @return ActiveQuery|Category
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return ActiveQuery|Type
     */
    public function getType()
    {
        return $this->hasOne(Type::class, ['id' => 'type_id'])
            ->via('category');
    }

    /**
     * @return ActiveQuery
     */
    public static function findInStock() : ActiveQuery
    {
        return static::find()->where(['>', 'in_stock', 0]);
    }

    public function getIsInStock() : bool
    {
        return $this->in_stock > 0;
    }

    /**
     * @return string Путь к обложке товара относительный (ссылка на сайте)
     */
    public function getImageUrl() : string
    {
        return "/images/goods/$this->image";
    }

    public function getImageFullUrl() : string
    {
        return Yii::getAlias('@front_url').$this->getImageUrl();
    }

}