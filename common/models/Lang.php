<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace common\models;


use Yii;
use yii\web\Cookie;

class Lang
{

    private static $_lang;

    public const LIST = [
        'English' => 'en-US',
        'Русский' => 'ru-RU',
    ];

    private static function init()
    {
        if (self::$_lang !== null) return;
        $cookie = Yii::$app->request->getCookies()->get('lang');
        self::$_lang = $cookie ? $cookie->value : 'en-US';
    }

    public static function set(string $lang)
    {
        if (!\in_array($lang, self::LIST)) {
            Yii::error('Lang not in list');
            return false;
        }
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'lang',
            'value' => $lang,
            'httpOnly' => true,
            'expire' => time()+2592000, // месяц
        ]));
        Yii::$app->language = $lang;
        self::$_lang = $lang;
        return true;
    }

    public static function get()
    {
        self::init();
        return self::$_lang;
    }

    public static function getShort()
    {
        return substr(self::get(), 0, 2);
    }

}