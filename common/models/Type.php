<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace common\models;

use yii\db\{ ActiveQuery, ActiveRecord };
use yii\helpers\ArrayHelper;


/**
 * Class Type
 * @package common\models
 *
 * @property integer $id
 * @property string $name_en
 * @property string $name_ru
 *
 * @property-read string $name
 *
 * @property Category[] $categories
 * @property Goods[] $goods
 */
class Type extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%type}}';
    }

    public function rules()
    {
        return [
            [['name_en', 'name_ru'], 'string', 'min' => 1, 'max' => 255, 'skipOnEmpty' => false],
        ];
    }

    /**
     * @return ActiveQuery|Category[]
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['type_id' => 'id']);
    }

    /**
     * @return ActiveQuery|Goods[]
     */
    public function getGoods()
    {
        return $this->hasMany(Goods::class, ['category_id' => 'id'])
            ->via('categories');
    }

    public function getName()
    {
        $attribute = 'name_'.Lang::getShort();
        return $this->$attribute;
    }


    private static $type_list_with, $type_list_without;

    /**
     * @return array Возвращает массив массивов:
     *   ['cat_id', 'cat', 'type_id', 'type']
     */
    private static function getTypeList($with_empty_types) : array
    {
        if ($with_empty_types) {
            if (static::$type_list_with !== null) return static::$type_list_with;
        } elseif (static::$type_list_without !== null) return static::$type_list_without;

        $cat = Category::tableName();
        $type = static::tableName();
        $lang = Lang::getShort();

        $query = static::find();
        if ($with_empty_types) $query->joinWith('categories', false);
        else $query->innerJoinWith('categories', false);

        $array = $query->select(["$type.id as type_id", "$type.name_$lang as type",
            "$cat.id as cat_id", "$cat.name_$lang as cat"])->asArray()->all();

        if ($with_empty_types) static::$type_list_with = $array;
        else static::$type_list_without = $array;

        return $array;
    }

    public static function getDropDownList($with_empty_types = true) : array
    {
        return ArrayHelper::map(static::getTypeList($with_empty_types), 'type_id', 'type');
    }

    public static function getFullDropDownList($with_empty_types = true) : array
    {
        $array = static::getTypeList($with_empty_types);

        $dropdown_list = ArrayHelper::map($array, 'cat_id', 'cat', 'type');

        if ($with_empty_types)
            foreach ($dropdown_list as $key => $val)
                unset($dropdown_list[$key]['']);

        return $dropdown_list;
    }

}