<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class News
 * @package common\models
 *
 * @property integer $id
 * @property string $title_en
 * @property string $title_ru
 * @property string $content_en
 * @property string $content_ru
 *
 * @property-read string $title
 * @property-read string $content
 */
class News extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%news}}';
    }

    public function rules()
    {
        return [
            [['title_en', 'title_ru'], 'string', 'min' => 1, 'max' => 255, 'skipOnEmpty' => false],
            [['content_en', 'content_ru'], 'string', 'skipOnEmpty' => false],
        ];
    }

    public function getContent(): string
    {
        $attribute = 'content_'.Lang::getShort();
        return $this->$attribute;
    }

    public function getTitle(): string
    {
        $attribute = 'title_'.Lang::getShort();
        return $this->$attribute;
    }

}