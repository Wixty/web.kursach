<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'MusiKo Shop',
    'homeUrl' => '/',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'csrfCookie' => [
                'httpOnly' => true,
            ],
        ],
        'user' => [
            'identityClass' => false, //'common\models\User',
            /*'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-frontend',
                'httpOnly' => true
            ],*/
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/',
            'rules' => [
                'index.shtml' => 'site/index',
                '<action:(news|about-us|contacts)>' => 'site/<action>',
                /*
                '<controller:[\d\w-]+\/?>/<action:[\d\w-]+\/?>' => '<controller>/<action>',
                */
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        /*
        'yandexMapsApi' => [
            'class' => \mirocow\yandexmaps\Api::class,
            'language' => $lang,
        ],
        */
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => \himiklab\yii2\recaptcha\ReCaptcha::class,
            'siteKey' => $params['reCaptcha']['site_key'],
            'secret' => $params['reCaptcha']['secret_key'],
        ],
    ],
    'params' => $params,

    'on beforeAction' => function() {
        // перед запуском любого экшена в начале установим сохранённый в куках язык
        $lang = \common\models\Lang::get();
        Yii::$app->language = $lang;
        Yii::$app->set('yandexMapsApi', [
            'class' => \mirocow\yandexmaps\Api::class,
            'language' => $lang,
        ]);
        Yii::$app->name = Yii::t('msg', 'MusiKo Shop');
    },
];
