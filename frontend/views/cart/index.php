<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\widgets\MaskedInput;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use himiklab\yii2\recaptcha\ReCaptcha;
use common\models\Order;

/* @var $this \yii\web\View */
/* @var $cart array */
/* @var $goods_in_cart common\models\Goods[] */
/* @var $order_form \frontend\models\OrderForm */



$this->title = Yii::t('msg', 'Cart');
$this->params['breadcrumbs'][] = $this->title;

$total_price = 0;
$total_count = 0;
$num = 1;

?>
<div class="cart">
    <h1 class="text-center"><?= $this->title ?></h1>
    <div class="col-lg-12" id="cart-panels">
        <?php if (count($goods_in_cart)) : ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center bold">
                        <?= Yii::t('msg', 'Your cart contains') ?>
                    </h3>
                </div>

                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <td>№</td>
                        <td><?= Yii::t('msg', 'Name') ?></td>
                        <td><?= Yii::t('msg', 'Photo') ?></td>
                        <td><?= Yii::t('msg', 'Price') ?></td>
                        <td><?= Yii::t('msg', 'Count') ?></td>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($goods_in_cart as $goods) :
                        $count = ($cart[$goods->id] ?? 0);
                        echo $this->render('_goods', compact('goods', 'count', 'num'));
                        $total_price += $goods->price * $count;
                        $total_count += $count;
                        $num++;
                    endforeach; ?>

                    </tbody>
                    <tfoot class="bold">
                    <tr>
                        <td colspan="3" class="table-total"><?= Yii::t('msg', 'Total') ?></td>
                        <td>$<?= $total_price ?></td>
                        <td><?= $total_count ?></td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title text-center bold">
                        <?= Yii::t('msg', 'To order goods, please fill in the following fields') ?>
                    </h3>
                </div>

                <?php $form = ActiveForm::begin([
                    'id' => 'order-form',
                ]);
                $form->on('beforeSubmit', '() => {return false}')?>

                <div class="panel-body">
                    <div class="col-sm-6">
                        <?= $form->field($order_form, 'name') ?>
                        <?= $form->field($order_form, 'address') ?>
                        <?= $form->field($order_form, 'telephone')->widget(MaskedInput::class, [
                            'mask'  => ['+9 (999) 999-99-99', '+9 (9999) 999-99-99'],
                        ]) ?>
                        <?= $form->field($order_form, 'email')->widget(MaskedInput::class, [
                            'clientOptions' => ['alias' => 'email']]) ?>
                    </div>
                    <div class="col-sm-6"><?= $form->field($order_form, 'pay_method')->widget(Select2::class, [
                            'data' => Order::getPayMethods(), 'hideSearch' => true,
                            'options' => ['prompt' => Yii::t('msg', Yii::t('msg', 'Choose one...'))],
                        ]) ?>
                        <?= $form->field($order_form, 'comment')->textarea(['rows' => 3]) ?>
                        <?= $form->field($order_form, 'reCaptcha')->widget(ReCaptcha::class) ?>
                    </div>
                </div>

                <div class="panel-footer clearfix">
                    <div class="col-xs-8 col-sm-7 col-md-6 summary">
                        <?= Yii::t('msg', 'Total price of the order: ${sum}', ['sum' => $total_price]) ?>
                    </div>
                    <div class="col-xs-4 col-sm-5 col-md-6">
                        <?= Html::submitButton(Yii::t('msg', Yii::t('msg', 'Place the order')),
                            ['class' => 'btn btn-success', 'name' => 'contact-button']) ?>
                    </div>
                </div>

                <?php ActiveForm::end() ?>
            </div>

        <?php else : ?>

            <div class="panel panel-default">
                <span class="cart-empty"><?= Yii::t('msg', 'Your cart is empty!') ?></span>
            </div>

        <?php endif; ?>
    </div>
</div>

<div id="alerts">
    <div id="buy-error" class="alert alert-danger">
        <?= Yii::t('msg', 'Something going wrong, please try again later.') ?>
    </div>
</div>