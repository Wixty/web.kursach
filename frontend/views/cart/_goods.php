<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use common\models\Goods;

/* @var $goods Goods */
/* @var $count integer */
/* @var $num integer */

?>

<tr>
    <td><?= $num ?></td>
    <td>
        <a href="<?= Url::to(['/goods', 'id' => $goods->id]) ?>" class="underline-disable">
            <?= $goods->name ?>
        </a>
    </td>
    <td>
        <div class="img-parent" style="height: 50px !important;">
            <?= Html::img($goods->imageUrl) ?>
        </div>
    </td>
    <td>$<?= $goods->price ?></td>
    <td>
        <?= Html::button('&ndash;', ['class' => 'btn btn-danger btn-xs', 'data-link' => 'remove', 'data-id' => $goods->id]) ?>
        <br class="visible-xs-inline">
        <span><?= $count ?></span>
        <br class="visible-xs-inline">
        <?= $count < Goods::MAX_COUNT_IN_CART ? Html::button('+', ['class' => 'btn btn-success btn-xs', 'data-link' => 'add', 'data-id' => $goods->id]) : '' ?>
    </td>
</tr>