<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\{ Html, Carousel };
use yii\helpers\{ ArrayHelper, Url };
use zxbodya\yii2\galleryManager\GalleryImage;

/* @var $goods common\models\Goods */
/* @var $gallery_images GalleryImage[] */


$type = $goods->category->type;
$category = $goods->category;

$this->title = $goods->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('msg', 'Goods Catalog'), 'url' => '/goods'];
$this->params['breadcrumbs'][] = ['label' => $type->name, 'url' => ['/goods', 'type' => $type->id]];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => ['/goods', 'type' => $type->id, 'category' => $category->id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="goods-one">

    <h1 class="text-center"><?= $this->title ?></h1>

    <div class="col-lg-5 col-md-6 bottom-space">
        <?php if (count($gallery_images)) {
            echo Carousel::widget([
                'items' => ArrayHelper::getColumn($gallery_images, function($img) {
                    /* @var $img GalleryImage */
                    return Html::img($img->getUrl('medium'));
                }),
                'controls' => [
                    Html::icon('chevron-left'),
                    Html::icon('chevron-right'),
                ],
            ]);
        } else {
            echo Html::img($goods->imageUrl);
        } ?>
    </div>

    <div class="col-lg-7 col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <span class="price"><?= $goods->price ?> $</span>
                <?php if ($goods->isInStock) : ?>
                    <span class="label label-success"><?= Yii::t('msg', 'In stock') ?></span>
                    <?= Html::a(Yii::t('msg', 'Add to cart'), '#',
                        ['class' => 'btn btn-success pull-right buy', 'data-id' => $goods->id, 'id' => 'buy']) ?>
                <?php else : ?>
                    <span class="label label-warning"><?= Yii::t('msg', 'Out of stock') ?></span>
                <?php endif; ?>
                <hr>
                <?= Yii::t('app', $goods->description); ?>
            </div>
        </div>
    </div>

</div>

<div id="alerts">
    <div id="buy-error" class="alert alert-danger">
        <?= Yii::t('msg', 'Something going wrong, please try again later.') ?>
    </div>
</div>