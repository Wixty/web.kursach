<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\widgets\ListView;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use kartik\form\ActiveForm;

/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel frontend\models\GoodsSearch */
/* @var $type_list_full array */
/* @var $type_list array */


$this->title = Yii::t('msg', 'Goods Catalog');

$category = Yii::$app->request->get('category');
$type = Yii::$app->request->get('type');

if (!$type) {
    $this->params['breadcrumbs'][] = $this->title;
} else {
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '/goods'];
    if (!$category) {
        $this->params['breadcrumbs'][] = $type_list[$type];
    } else {
        $this->params['breadcrumbs'][] = ['label' => $type_list[$type], 'url' => ['/goods', 'type' => $type]];
        $this->params['breadcrumbs'][] = $type_list_full[$type_list[$type]][$category];
    }
}

?>
<div class="goods-all">
    <h1 class="text-center"><?= $this->title ?></h1>

    <div class="col-lg-12 filter-bar clearfix">

        <?php $form = ActiveForm::begin([
            'action' => '/goods',
            'method' => 'get',
            'options' => ['data-pjax' => true],
        ]); ?>

        <div class="col-lg-12 col-sm-12">

            <div class="col-lg-3 col-sm-6">
                <?= $form->field($searchModel, 'name')->label(false)
                    ->textInput(['placeholder' => Yii::t('msg', 'Name'),
                        'name' => 'name', 'class' => 'form-control text-center']) ?>
            </div>
            <div class="col-lg-3 col-sm-6">
                <?= $form->field($searchModel, 'type')->label(false)
                    ->widget(Select2::class, [
                        'data' => $type_list ?? [], 'hideSearch' => true,
                        'options' => ['placeholder' => Yii::t('msg', 'Type'), 'name' => 'type'],
                    ]) ?>
            </div>
            <div class="col-lg-3 col-sm-6">
                <?= $form->field($searchModel, 'category')->label(false)
                    ->widget(Select2::class, [
                        'data' => $type_list_full, 'hideSearch' => true,
                        'options' => ['placeholder' => Yii::t('msg', 'Category'), 'name' => 'category'],
                    ]) ?>
            </div>
            <div class="col-lg-1 col-sm-3 filter-checkbox">
                <?= $form->field($searchModel, 'stock')->label(Yii::t('msg', 'Stock'))
                    ->widget(CheckboxX::class, [
                        'options' => ['name' => 'stock'],
                        'pluginOptions' => [ 'threeState' => false, ]
                    ]) ?>
            </div>
            <div class="col-lg-2 col-sm-3 filter-button">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('msg', 'Filter'), ['class' => 'btn btn-success']) ?>
                    <?= Html::button(Yii::t('msg', 'Reset'), ['class' => 'btn btn-warning', 'id' => 'reset-filter']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end() ?>
    </div>

    <div class="col-lg-12">
        <?= ListView::widget([
            'summary' => false,
            'layout' => "{summary}\n{pager}\n{items}\n{pager}",
            'dataProvider' => $dataProvider,
            'itemView' => '_goods',
            'emptyTextOptions' => ['class' => 'empty text-center price'],
        ]); ?>
    </div>

</div>