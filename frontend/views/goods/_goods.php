<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use common\models\Goods;

/* @var $key mixed The key value associated with the data item */
/* @var $index integer The zero-based index of the data item in the items array returned by the data provider */
/* @var $widget yii\widgets\ListView Widget instance */
/* @var $model Goods */

?>
<div class="col-lg-3 col-md-4 col-sm-6">
    <div class="panel panel-default item">
        <div class="panel-body">
            <a href="<?= Url::to(['/goods', 'id' => $model->id]) ?>" class="underline-disable">
                <div class="img-parent">
                    <?= Html::img($model->imageUrl) ?>
                </div>
                <span class="goods-title"><b><?= $model->getName() ?></b></span>
                <span class="price price-br">$<?= $model->price ?></span>
                <span class="label label-<?= $model->isInStock ? 'success' : 'warning' ?> label-stock">
                    <?= $model->isInStock ? Yii::t('msg', 'In stock') : Yii::t('msg', 'Out of stock') ?>
                </span>
            </a>
        </div>
    </div>
</div>