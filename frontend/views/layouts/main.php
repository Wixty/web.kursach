<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use rmrevin\yii\fontawesome\{ FAR, FAS };
use kartik\nav\NavX;
use common\models\{ Lang, Type };
use common\widgets\Alert;
use frontend\assets\AppAsset;

AppAsset::register($this);

$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    $lang_items = [];
    $current_lang = Lang::get();
    foreach (Lang::LIST as $label => $lang) {
        $lang_items[] = [
            'label' => $label, 'url' => '#', 'active' => $lang === $current_lang,
            'linkOptions' => ['data-lang' => $lang, 'class' => 'text-center']
        ];
    }

    $catalog_items = [
        ['label' => Yii::t('msg', 'All goods'), 'url' => ['/goods']],
        '<li class="divider"></li>',
    ];
    foreach (Type::find()->with('categories')->all() as $type) {
        if (empty($type->categories)) continue;
        $items = [
            ['label' => Yii::t('msg', 'All {type}', ['type' => $type->name]),
                'url' => ['/goods', 'type' => $type->id]],
            '<li class="divider"></li>',
        ];
        foreach ($type->categories as $category)
            $items[] = ['label' => $category->name, 'url' => ['/goods', 'type' => $type->id, 'category' => $category->id]];
        $catalog_items[] = ['label' => $type->name, 'items' => $items];
    }

    $leftItems = [
        ['label' => Yii::t('msg', 'Home'), 'url' => ['/']],
        ['label' => Yii::t('msg', 'News and Deals'), 'url' => ['/news']],
        ['label' => Yii::t('msg', 'Goods Catalog'), 'items' => $catalog_items],
    ];
    $rightItems = [
        ['label' => Yii::t('msg', 'About us'), 'url' => ['/site/about-us']],
        ['label' => Yii::t('msg', 'Contacts'), 'url' => ['/site/contacts']],
        ['label' => FAR::icon('flag').Yii::t('msg', 'Language'),
            'items' => $lang_items, 'options' => ['id' => 'lang']],
        ['label' => FAS::icon('shopping-cart').Yii::t('msg', 'Cart'),
            /*'items' => $cart_items,*/ 'url' => ['/cart']],
    ];

    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
            'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo NavX::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $leftItems,
        'encodeLabels' => false,
    ]);
    echo NavX::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $rightItems,
        'encodeLabels' => false,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => $this->params['breadcrumbs'] ?? [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<?php
$host = Yii::$app->urlManager->hostInfo;
$url = preg_replace('/\/\/s\./', '//a.s.', $host, 1, $count);
if ($count < 1) $url = preg_replace('/\/\/shop\./', '//admin.shop.', $host, 1);
?>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
        <p class="text-center"><?= Html::a(Yii::t('msg', 'Admin Panel'), $url, ['style' => 'margin:0 20px']) ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
