<?php

use yii\bootstrap\{ Carousel, Html };

/* @var $this yii\web\View */

$this->title = Yii::$app->name;

?>
<div class="site-index">
    <h1 class="text-center">
        <?= Yii::t('msg', 'Welcome to MusiKo shop!') ?>
    </h1>

    <?= Carousel::widget([
        'items' => [
            Html::img('/images/carousel/12.jpg'),
            Html::img('/images/carousel/10.jpg'),
            Html::img('/images/carousel/11.jpg'),
        ],
        'controls' => [
            Html::icon('chevron-left'),
            Html::icon('chevron-right'),
        ],
    ]) ?>

    <div class="jumbotron">
        <h1><?= Yii::t('msg', 'Choose the Best!') ?></h1>
        <a class="btn btn-lg btn-success" href="/goods">
            <?= Yii::t('msg', 'Goods Catalog') ?>
        </a>
    </div>

</div>
