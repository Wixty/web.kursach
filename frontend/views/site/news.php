<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $news_on_page \common\models\News[] */
/* @var $pages \yii\data\Pagination */


$this->title = Yii::t('msg', 'News and Deals');

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-news">
    <h1 class="text-center"><?= $this->title ?></h1>

    <div class="col-lg-12">
        <hr>
        <?php foreach ($news_on_page as $news) : ?>
            <article class="clearfix">
                <h2 class="text-center bottom-space"><?= $news->title ?></h2>
                <div class="news-content">
                    <?= $news->content ?>
                </div>
            </article>
            <hr>
        <?php endforeach; ?>

        <div class="text-center">
            <?= LinkPager::widget([
                'pagination' => $pages,
            ]) ?>
        </div>
    </div>

</div>