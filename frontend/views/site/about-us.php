<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $page \common\models\Page */


$this->title = $page->title;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-about">
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <?= $page->content ?>

</div>
