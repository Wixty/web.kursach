<?php

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\web\{ AssetBundle, JqueryAsset, YiiAsset };


/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css',
    ];

    public $js = [
        'js/custom.js',
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        JqueryAsset::class,
        \rmrevin\yii\fontawesome\AssetBundle::class,
    ];
}
