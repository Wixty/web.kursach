<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */


namespace frontend\models;

use Yii;
use yii\base\Model;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use common\models\Order;


class OrderForm extends Model
{
    public $name, $address, $telephone, $email, $pay_method, $comment, $reCaptcha;

    public function rules()
    {
        return [
            [['name', 'address', 'telephone', 'email', 'pay_method', 'reCaptcha'], 'required'],
            [['name', 'address', 'email'], 'string', 'min' => 7],
            [['telephone'], 'string', 'min' => 18],
            [['comment'], 'string'],
            ['email', 'email'],
            ['pay_method', 'in', 'range' => Order::getPayMethodsIDs()],
            ['reCaptcha', ReCaptchaValidator::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('msg', 'Full name'),
            'address' => Yii::t('msg', 'Full address'),
            'telephone' => Yii::t('msg', 'Contact telephone'),
            'pay_method' => Yii::t('msg', 'Payment method'),
            'email' => Yii::t('msg', 'Email'),
            'comment' => Yii::t('msg', 'Comment'),
            'reCaptcha' => Yii::t('msg', 'Bot Check'),
        ];
    }

    public function beforeValidate()
    {
        $this->telephone = str_replace('_', '', $this->telephone);
        return parent::beforeValidate();
    }

}