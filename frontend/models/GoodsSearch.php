<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace frontend\models;

use yii\data\ActiveDataProvider;
use common\models\{ Category, Goods, Lang };


/**
 * Class GoodsSearch
 * @package frontend\models
 *
 * @property integer $type
 * @property integer $category
 * @property string $name
 * @property boolean $stock
 */
class GoodsSearch extends Goods
{

    public $type, $category, $name, $stock;

    public function rules()
    {
        return [
            [['name'], 'string'],
            [['stock'], 'boolean'],
            [['type', 'category'], 'integer'],
        ];
    }

    public function search($params)
    {
        $query = parent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if (!($this->load($params, '') && $this->validate())) {
            return $dataProvider;
        }

        $goodz = Goods::tableName();
        $catz = Category::tableName();

        $query->andFilterWhere(['category_id' => $this->category])
            ->andFilterWhere(['like', "$goodz.name_".Lang::getShort(), $this->name]);

        if (!empty($this->type)) {
            $query->innerJoinWith('category')
                ->andFilterWhere(['type_id' => $this->type])
                ->select(["$goodz.*", "$catz.`type_id`"]);
        }
        if ($this->stock) {
            $query->andFilterWhere(['>', 'in_stock', 0]);
        }

        return $dataProvider;
    }

}