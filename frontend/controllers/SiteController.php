<?php

namespace frontend\controllers;

use Yii;
use yii\data\Pagination;
use yii\web\{ Controller, ErrorAction, NotFoundHttpException };
use mirocow\yandexmaps\{ Map, objects\Placemark };
use common\models\{ News, Page, Lang };


/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionNews()
    {
        $pages = new Pagination([
            'totalCount' => News::find()->count(),
            'defaultPageSize' => 1,
            'route' => '/news',
        ]);

        $news_on_page = News::find()
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('id DESC')
            ->all();

        return $this->render('news', compact('news_on_page', 'pages'));
    }


    public function actionAboutUs()
    {
        $page = Page::findOne(['url' => 'about-us']);
        if (!$page) throw new NotFoundHttpException();

        return $this->render('about-us', compact('page'));
    }


    public function actionContacts()
    {
        $page = Page::findOne(['url' => 'contacts']);
        if (!$page) throw new NotFoundHttpException();

        $placemark = new Placemark([59.955078, 30.245428], [
            'iconCaption' => Yii::t('msg', Yii::$app->name),
        ], [
            'draggable' => false,
            'preset' => 'islands#redGlyphIcon',
            'iconGlyph' => 'music',
            'iconGlyphColor' => 'red',
        ]);
        $map = new Map('yandex_map', [
            'center' => [59.955078, 30.245428],
            'zoom' => 16,
            //'behaviors' => ['default', 'scrollZoom'],
            'type' => 'yandex#map', // схема
        ]);
        $map->addObject($placemark);

        return $this->render('contacts', compact('page', 'map'));
    }


    public function actionChangeLang()
    {
        if (!Yii::$app->request->isAjax) return false;

        $lang = Yii::$app->request->post('lang');
        if ($lang) $result = Lang::set($lang);
        else Yii::error('Lang not sent');

        return json_encode(['success' => $result ?? false]);
    }

}
