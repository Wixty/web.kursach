<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace frontend\controllers;

use Yii;
use yii\web\{ Controller, NotFoundHttpException };
use common\models\{ Goods, Type };
use frontend\models\GoodsSearch;


class GoodsController extends Controller
{

    public function actionIndex($id = null)
    {
        if ($id !== null)
            return $this->renderGoodsPage((int)$id);

        return $this->renderAllGoodsPage();
    }

    protected function renderAllGoodsPage()
    {
        $searchModel = new GoodsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $type_list = Type::getDropDownList(false);
        $type_list_full = Type::getFullDropDownList(false);

        return $this->render('index_all', compact('dataProvider', 'searchModel', 'type_list', 'type_list_full'));
    }

    protected function renderGoodsPage(int $id)
    {
        $goods = $id > 0 ? Goods::findOne($id) : null;
        if (!$goods) throw new NotFoundHttpException(Yii::t('msg', 'Sorry, goods is not found.'));

        $gallery_images = $goods->getGalleryImages();

        return $this->render('index_one', compact('goods', 'gallery_images'));
    }

}