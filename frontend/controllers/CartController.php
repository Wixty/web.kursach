<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace frontend\controllers;

use Yii;
use yii\web\{ Controller, MethodNotAllowedHttpException };
use common\models\{ Cart, Order };
use frontend\models\OrderForm;


class CartController extends Controller
{

    public function actionIndex()
    {
        [$cart, $goods_in_cart] = Cart::checkAndGet();
        $order_form = new OrderForm();

        return $this->render('index', compact('cart', 'goods_in_cart', 'order_form'));
    }

    public function actionAdd()
    {
        if (!Yii::$app->request->isPost)
            throw new MethodNotAllowedHttpException();

        $id = (int) Yii::$app->request->post('id');
        return json_encode(Cart::add($id));
    }

    public function actionRemove()
    {
        if (!Yii::$app->request->isPost)
            throw new MethodNotAllowedHttpException();

        $id = (int) Yii::$app->request->post('id');
        return json_encode(Cart::remove($id));
    }

    public function actionPlaceOrder()
    {
        if (!Yii::$app->request->isPost)
            throw new MethodNotAllowedHttpException();

        $order_form = new OrderForm();
        if ($order_form->load(Yii::$app->request->post('OrderForm'), '') && $order_form->validate()) {

            // создаём, заполняем и сохраняем заказ
            $order = new Order();
            $order->ip = Yii::$app->request->userIP;
            if ($order->load($order_form->getAttributes(), '') && $order->save()) {

                // сохраним корзину (создадим для каждого товара по одной записи в бд)
                if (Cart::saveForOrder($order->id)) {
                    return json_encode([
                        'status' => 'success',
                        'message' => Yii::t('msg', 'The order was successfully placed. Our manager will contact you within an hour.'),
                        'html' => '<div class="panel panel-default"><span class="cart-empty">'
                            . Yii::t('msg', 'Your cart is empty!')
                            . '</span></div>',
                    ]);
                }
                // если сохранение корзины не удалось, откатываем заказ
                $order->delete();
                Yii::error('Cart::save() error!');

            } else Yii::error(['$order->load() error!', $order->errors]);

            return json_encode([
                'status' => 'danger',
                'message' => Yii::t('msg', 'An error was occurred while creating the order. Please try again a later.'),
            ]);

        } else Yii::error(['$order_form->validate() error!', $order_form->errors]);

        return json_encode([
            'status' => 'warning',
            'message' => Yii::t('msg', 'There was an error creating the order. Check the correctness of filling the form.'),
        ]);
    }

}