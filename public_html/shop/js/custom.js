/*
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

$(function () {

    $('#lang').on('click', 'li:not(.active)>a', event => {
        // смена языка ajax
        let lang = $(event.target).data('lang');
        if (!lang) return;
        $.ajax({
            url: '/site/change-lang',
            method: 'POST',
            data: { lang: lang, },
            dataType: 'json',
            success: data => {
                if (data.success !== true)
                    console.log('Change-lang: data error');
                else location.reload();
            },
            error: () => console.log('Change-lang: ajax error'),
        });
    });

    $('#reset-filter').on('click', () => {
        // полный сброс фильтров
        $('#goodssearch-name').val('');
        $('#goodssearch-category, #goodssearch-type').select2('val', 0);
        let stock = $('#goodssearch-stock');
        if (stock.val() == 1) stock.click();
    });

    $('#goodssearch-type').on('change', () => {
        // сброс фильтра категорий при переключении фильтра типов
        $('#goodssearch-category').select2('val', 0);
    });

    $('#goodssearch-category').on('change', () => {
        // переключение фильтра типов при переключении фильтра категорий
        let cat_select = $('#goodssearch-category'),
            category_id = cat_select.select2('val'),
            type = cat_select.find('optgroup:has(option:selected)').attr('label'),
            type_select = $('#goodssearch-type');
        if (type && type !== type_select.find('option:selected').text()) {
            let id = type_select.find('option').filter(function() {
                return $(this).text() === type;
            }).val();
            type_select.select2('val', id);
            cat_select.select2('val', category_id);
        }
        return true;
    });

    $('#buy').on('click', event => {
        // ajax покупка
        $.ajax({
            url: '/cart/add',
            method: 'POST',
            data: {id: $(event.target).data('id')},
            dataType: 'json',
            success: data => show_alert(data.status, data.message),
            error: () => show_error(),
        });
    });

    function show_alert(type, message) {
        // уведомление заданного типа с заданным содержанием
        let alert = $('<div class="alert alert-'+type+'">'+message+'</div>');
        alert.appendTo('#alerts').fadeIn().delay(7000).slideUp();
        setTimeout(() => alert.remove(), 7800);
    }

    function show_error() {
        // ошибка при ajax запросе
        $('#buy-error').clone().appendTo('#alerts').fadeIn().delay(5000).slideUp();
    }

    $('#alerts').on('click', '.alert', event => {
        // скрытие уведомления при клике
        $(event.target).slideUp({queue: false});
    });

    $('.cart table').on('click', '.btn', event => {
        // добавление/удаление в корзине
        let btn = $(event.target),
            link = btn.data('link'),
            id = btn.data('id');
        if (!link || !id) return;
        $.ajax({
            url: '/cart/'+link,
            method: 'POST',
            data: {id: id},
            dataType: 'json',
            // если успешно, обновим
            success: () => location.reload(),
            // не успешно - ошибка
            error: () => show_error(),
        });
    });

    $('#order-form').on('beforeSubmit', event => {
        // оформление заказа
        let form = $(event.target);
        $.ajax({
            url: '/cart/place-order',
            method: 'POST',
            data: form.serialize(),
            dataType: 'json',
            success: data => {
                // уведомление + если прислано, заменим содержимое корзины
                show_alert(data.status, data.message);
                if (data.html) $('#cart-panels').replaceWith(data.html);
            },
            error: () => show_error(),
        });
        return false;
    });

});
