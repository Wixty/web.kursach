<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\helpers\{ ArrayHelper, FileHelper, VarDumper };
use yii\web\{ Controller, ForbiddenHttpException, MethodNotAllowedHttpException, NotFoundHttpException, UploadedFile };
use zxbodya\yii2\galleryManager\GalleryManagerAction;
use common\models\{ Category, Type, Goods };
use backend\models\{ GoodsSearch, Admin };

class GoodsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'gallery-api' => [
                'class' => GalleryManagerAction::class,
                'types' => [ 'goods' => Goods::class ],
            ],
            'images-get' => [
                'class' => \vova07\imperavi\actions\GetImagesAction::class,
                'url' => Yii::getAlias('@front_url/images/goods/upload'),
                'path' => '@front_web/images/news/',
            ],
            'image-upload' => [
                'class' => \vova07\imperavi\actions\UploadFileAction::class,
                'url' => Yii::getAlias('@front_url/images/goods/upload'),
                'path' => '@front_web/images/news/',
            ],
            'file-delete' => [
                'class' => \vova07\imperavi\actions\DeleteFileAction::class,
                'url' => Yii::getAlias('@front_url/images/goods/upload'),
                'path' => '@front_web/images/news/',
            ],
        ];
    }


    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;

        /** @var $identity Admin Залогиненый админ */
        $identity = Yii::$app->user->identity;

        if(!$identity->canEditGoods)
            throw new ForbiddenHttpException('You do not have rights to do this!');

        return true;
    }


    public function actionIndex()
    {
        $searchModel = new GoodsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $type_list = Type::getDropDownList();
        $category_list = Category::getDropDownList();

        return $this->render('index', compact('searchModel', 'dataProvider', 'type_list', 'category_list'));
    }


    public function actionEdit($id = 0)
    {
        $id = (int)$id;

        $goods = $id > 0 ? Goods::findOne($id) : new Goods();
        if(!$goods) throw new NotFoundHttpException('Goods not found!');

        $old_image = $goods->image;
        if (Yii::$app->request->isPost && $goods->load(Yii::$app->request->post())) {
            $goods->setScenario('edit');
            $goods->image_file = UploadedFile::getInstance($goods, 'image');
            $goods->image = '';
            if (!empty($goods->image_file) && $goods->validate('image_file')) {
                $path = Yii::getAlias('@front_web/images/goods/');
                if (is_dir($path) || FileHelper::createDirectory($path)) {
                    $file_name = crc32((string)$goods->image_file) . '.'. $goods->image_file->extension;
                    if ($goods->image_file->saveAs($path . $file_name))
                        $goods->image = $file_name;
                    else Yii::error('SavePic error');
                } else Yii::error('Directory error');
            } else Yii::error('image_file error');

            $goods->setScenario('default');
            if (empty($goods->image)) $goods->image = $old_image;
            if (empty($goods->category_id)) $goods->category_id = null;

            if ($goods->save()) {
                Yii::$app->session->setFlash('success', 'Goods settings is saved successfully!');
                return $this->redirect('/goods');
            } else Yii::$app->session->setFlash('error', VarDumper::export($goods->getErrors()));
        }

        $dropdown_list = Type::getFullDropDownList(true);

        return $this->render('edit', compact('goods', 'dropdown_list'));
    }

    public function actionGallery($id)
    {
        $id = (int)$id;

        $goods = $id > 0 ? Goods::findOne($id) : null;
        if (!$goods) throw new NotFoundHttpException('Goods not found!');

        return $this->render('gallery', compact('goods'));
    }


    public function actionDelete($id)
    {
        if (!Yii::$app->request->isPost)
            throw new MethodNotAllowedHttpException();

        $id = (int)$id;
        $good = $id > 0 ? Goods::findOne($id) : null;
        if($good) $good->delete();

        return $this->redirect('/goods');
    }

}