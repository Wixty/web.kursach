<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\{ Controller, BadRequestHttpException, ForbiddenHttpException, MethodNotAllowedHttpException, NotFoundHttpException };
use backend\models\{ Admin, AdminSearch };


class AdminsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new AdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $identity = Yii::$app->user->identity;

        return $this->render('index', compact('searchModel', 'dataProvider', 'identity'));
    }


    /**
     * Смена мыла, логина, (опционально) пароля юзера.
     * СуперАдмин может редактировать данные любого юзера, все остальные - только свои.
     */
    public function actionEdit($id)
    {
        $id = (int)$id;

        /** @var $identity Admin Залогиненый админ */
        /** @var $admin Admin Редактируемый админ */

        $identity = Yii::$app->user->identity;

        if ($identity->id === $id) { // изменение своих данных
            $admin = $identity;
        } elseif($identity->canEditAdmins) { // изменение чужих данных
            $admin = Admin::findOne($id);
        } else {
            throw new ForbiddenHttpException('You do not have rights to do this!');
        }
        if (!$admin) return new NotFoundHttpException('Admin not found!');

        if (Yii::$app->request->isPost) { // валидация и сохранение
            $session = Yii::$app->session;
            $post = Yii::$app->request->post();

            if (!isset($post['psw'], $post['chk']))
                throw new BadRequestHttpException('Method received wrong parameters.');

            if (!$admin->load($post) || !$admin->save()) {
                $session->setFlash('error_in_load', 'Sorry, an error has occurred. Maybe someone taken this login already?');
                Yii::error(json_encode(['errors' => $admin->getErrors()]));
            } elseif (empty($post['psw'])) { // логин и мыло сохранились, пароль менять не надо
                return $this->redirect(['/admins']);
            } elseif (mb_strlen($post['psw']) < 6) { // проверки на смену пароля
                $session->setFlash('error_in_new', 'New password is too short.');
            } elseif ($post['psw'] !== $post['chk']) {
                $session->setFlash('error_in_check', 'New password and confirmation not equal.');
            } else {
                $admin->setPassword($post['psw']); // запоминаем хэш пароля
                $admin->generateAuthKey(); // и генерируем новый ключ для входа по кукам
                if ($admin->save()) {
                    if ($identity->id === $admin->id) { // если изменён собственный пароль
                        Yii::$app->user->logout(); // разлогиниваем админа
                        return $this->goHome(); // и переходим на страницу входа
                    }
                    $session->setFlash('success', 'Saved successfully.');
                    return $this->redirect('/admins'); // иначе назад, к списку админов
                }
                // если валидация провалилась, вернём список ошибок
                $session->setFlash('errors', VarDumper::export($admin->getErrors()));
            }
        }

        return $this->render('edit', compact('admin'));
    }


    public function actionAdd()
    {
        /** @var $identity Admin */
        $identity = Yii::$app->user->identity;
        if (!$identity->canCreateAdmins)
            throw new ForbiddenHttpException('You do not have rights to do this!');

        $admin = new Admin(['created_at' => time(), 'status' => Admin::STATUS_MODERATOR]);

        if (Yii::$app->request->isPost) {
            $session = Yii::$app->session;
            $post = Yii::$app->request->post();

            if (!isset($post['psw'], $post['chk']))
                throw new BadRequestHttpException('Method received wrong parameters.');

            if (mb_strlen($post['psw']) < 6) {
                $session->setFlash('error_in_new', 'Password is too short.');
            } elseif ($post['psw'] !== $post['chk']) {
                $session->setFlash('error_in_check', 'Password and confirmation are not equal.');
            } elseif ($admin->load($post)) {
                $admin->setPassword($post['psw']);
                $admin->generateAuthKey();
                if ($admin->save()) {
                    return $this->redirect(['/admins']);
                }
                $session->setFlash('errors', VarDumper::export($admin->getErrors()));
            } else return json_encode(['checks' => 'load fail']);
        }
        return $this->render('add', compact('admin'));
    }


    public function actionChangeStatus($id, $direction)
    {
        /** @var Admin $identity Залогиненый админ */
        $identity = Yii::$app->user->identity;

        if (!$identity->canEditStatus)
            throw new ForbiddenHttpException('You do not have rights to do this!');
        if ($identity->id === $id)
            throw new ForbiddenHttpException('You can not do this with your account!');
        if (!Yii::$app->request->isPost)
            throw new MethodNotAllowedHttpException();
        if ($direction !== '+' && $direction !== '-')
            throw new BadRequestHttpException('Bad direction for new status.');

        $id = (int)$id;

        /** @var Admin $admin Редактируемый админ */
        $admin = Admin::findOne($id);
        if(!$admin) throw new NotFoundHttpException('Admin not found!');

        $found = $prev = $next = false;
        foreach (array_keys(Admin::$statuses) as $status) {
            if ($found) { $next = $status; break; }
            if ($admin->status === $status) $found = true;
            else $prev = $status;
        }
        if ($direction === '+') {
            if ($next === false) throw new BadRequestHttpException('Max status already.');
            $admin->updateAttributes(['status' => $next]);
        } else { //elseif ($direction === '-') {
            if ($prev === false) throw new BadRequestHttpException('Min status already.');
            $admin->updateAttributes(['status' => $prev]);
        }
        Yii::$app->session->setFlash('success', 'The status has been changed successfully.');

        return $this->redirect(['/admins']);
    }

}