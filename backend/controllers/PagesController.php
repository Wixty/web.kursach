<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\controllers;


use common\models\Page;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\{ Admin, PageSearch };
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class PagesController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'images-get' => [
                'class' => \vova07\imperavi\actions\GetImagesAction::class,
                'url' => Yii::getAlias('@front_url/images/pages/'),
                'path' => '@front_web/images/pages/',
            ],
            'image-upload' => [
                'class' => \vova07\imperavi\actions\UploadFileAction::class,
                'url' => Yii::getAlias('@front_url/images/pages/'),
                'path' => '@front_web/images/pages/',
            ],
            'file-delete' => [
                'class' => \vova07\imperavi\actions\DeleteFileAction::class,
                'url' => Yii::getAlias('@front_url/images/pages/'),
                'path' => '@front_web/images/pages/',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;

        /** @var $identity Admin Залогиненый админ */
        $identity = Yii::$app->user->identity;

        if (!$identity->canEditPages)
            throw new ForbiddenHttpException('You do not have rights to do this!');

        return true;
    }

    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    public function actionEdit($id)
    {
        $id = (int)$id;

        $page = Page::findOne($id);
        if (!$page) return new NotFoundHttpException('Admin not found!');

        if (Yii::$app->request->isPost)
            if ($page->load(Yii::$app->request->post()) && $page->save())
                return $this->redirect('/pages');

        return $this->render('edit', compact('page'));
    }

}