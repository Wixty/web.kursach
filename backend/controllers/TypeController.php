<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\{ Controller, ForbiddenHttpException, MethodNotAllowedHttpException, NotFoundHttpException };
use backend\models\{ Admin, TypeSearch };
use common\models\Type;


class TypeController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;

        /** @var $identity Admin Залогиненый админ */
        $identity = Yii::$app->user->identity;

        if (!$identity->canEditGoods)
            throw new ForbiddenHttpException('You do not have rights to do this!');

        return true;
    }


    public function actionIndex()
    {
        $searchModel = new TypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    public function actionEdit($id = 0)
    {
        $id = (int)$id;

        $type = $id > 0 ? Type::findOne($id) : new Type();
        if (!$type) throw new NotFoundHttpException('Type is not found!');

        if (Yii::$app->request->isPost) {
            if ($type->load(Yii::$app->request->post()) && $type->save()) {
                Yii::$app->session->setFlash('success', 'Type is saved successfully!');
                return $this->redirect('/type');
            } else Yii::$app->session->setFlash('error', VarDumper::export($type->getErrors()));
        }

        return $this->render('edit', compact('type'));
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->request->isPost)
            throw new MethodNotAllowedHttpException();

        $id = (int)$id;
        $type = $id > 0 ? Type::findOne($id) : null;
        if($type) $type->delete();

        return $this->redirect('/type');
    }

}