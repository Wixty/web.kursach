<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\{ Controller, ForbiddenHttpException, MethodNotAllowedHttpException, NotFoundHttpException };
use common\models\News;
use backend\models\{ Admin, NewsSearch };


class NewsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'images-get' => [
                'class' => \vova07\imperavi\actions\GetImagesAction::class,
                'url' => Yii::getAlias('@front_url/images/news/'),
                'path' => '@front_web/images/news/',
            ],
            'image-upload' => [
                'class' => \vova07\imperavi\actions\UploadFileAction::class,
                'url' => Yii::getAlias('@front_url/images/news/'),
                'path' => '@front_web/images/news/',
            ],
            'file-delete' => [
                'class' => \vova07\imperavi\actions\DeleteFileAction::class,
                'url' => Yii::getAlias('@front_url/images/news/'),
                'path' => '@front_web/images/news/',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;

        /** @var $identity Admin Залогиненый админ */
        $identity = Yii::$app->user->identity;

        if(!$identity->canEditNews)
            throw new ForbiddenHttpException('You do not have rights to do this!');

        return true;
    }

    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    public function actionEdit($id = 0)
    {
        $id = (int)$id;

        $news = $id > 0 ? News::findOne($id) : new News();
        if (!$news) throw new NotFoundHttpException('News not found!');

        if (Yii::$app->request->isPost)
            if ($news->load(Yii::$app->request->post()) && $news->save())
                return $this->redirect('/news');

        return $this->render('edit', compact('news'));
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->request->isPost)
            throw new MethodNotAllowedHttpException();

        $id = (int)$id;
        $news = $id > 0 ? News::findOne($id) : null;
        if ($news) $news->delete();

        return $this->redirect('/news');
    }

}