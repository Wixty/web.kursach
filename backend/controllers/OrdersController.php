<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\{ Controller, ForbiddenHttpException, NotFoundHttpException };
use backend\models\{ Admin, OrderSearch };
use common\models\Order;


class OrdersController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;

        /** @var $identity Admin Залогиненый админ */
        $identity = Yii::$app->user->identity;

        if (!$identity->canEditOrders)
            throw new ForbiddenHttpException('You do not have rights to do this!');

        return true;
    }

    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $status_list = Order::getStatuses();

        return $this->render('index', compact('searchModel', 'dataProvider', 'status_list'));
    }

    public function actionShow($id)
    {
        $id = (int)$id;
        $order = $id > 0 ? Order::findOne($id) : null;
        if (!$order) return new NotFoundHttpException('Order not found!');

        if (Yii::$app->request->isPost) {
            $order->status = Yii::$app->request->post('Order')['status'];
            if ($order->update())
                $this->redirect('/orders');
            else Yii::error(['$order->update failed!', $order->errors, $order->attributes]);
        }

        $status_list = Order::getStatuses();
        $pay_list = Order::getPayMethods();

        $carts = $order->getCarts()
            ->with('goods', 'goods.category', 'goods.category.type')->all();

        return $this->render('show', compact('order', 'status_list', 'pay_list', 'carts'));
    }

}