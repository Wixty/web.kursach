<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\{ ArrayHelper, VarDumper };
use yii\web\{ Controller, ForbiddenHttpException, MethodNotAllowedHttpException, NotFoundHttpException };
use backend\models\{ CategorySearch, Admin };
use common\models\{ Type, Category };


class CategoryController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;

        /** @var $identity Admin Залогиненый админ */
        $identity = Yii::$app->user->identity;

        if (!$identity->canEditGoods)
            throw new ForbiddenHttpException('You do not have rights to do this!');

        return true;
    }


    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $type_list = Type::getDropDownList();

        return $this->render('index', compact('searchModel', 'dataProvider', 'type_list'));
    }

    public function actionEdit($id = 0)
    {
        $id = (int)$id;

        $category = $id > 0 ? Category::findOne($id) : new Category();
        if (!$category) throw new NotFoundHttpException('Category not found!');

        if (Yii::$app->request->isPost) {
            if ($category->load(Yii::$app->request->post()) && $category->save()) {
                Yii::$app->session->setFlash('success', 'Category is saved successfully!');
                return $this->redirect('/category');
            } else Yii::$app->session->setFlash('error', VarDumper::export($category->getErrors()));
        }

        $type_list = Type::getDropDownList();

        return $this->render('edit', compact('category', 'type_list'));
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->request->isPost)
            throw new MethodNotAllowedHttpException();

        $id = (int)$id;
        $category = $id > 0 ? Category::findOne($id) : null;
        if($category) $category->delete();

        return $this->redirect('/category');
    }

}