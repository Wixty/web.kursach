<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use kartik\form\ActiveForm;

/* @var $this \yii\web\View */
/* @var $news \common\models\News  */


$this->title = $news->id > 0 ? 'Edit News' : 'Add News';

$this->params['breadcrumbs'][] = ['label' => 'All News', 'url' => '/news'];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= $news->id > 0 ? "{$this->title} '{$news->title_en}'" : $this->title ?></h1>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-12">
        <?= $form->field($news, 'title_en')->textInput(['max' => 255, 'required' => true]); ?>

        <?= $form->field($news, 'content_en')->widget(\vova07\imperavi\Widget::class, [
            'settings' => [
                'imageUpload' => '/news/image-upload',
                'imageDelete' => '/news/file-delete',
                'imageManagerJson' => '/news/images-get',
                'plugins' => [ 'fullscreen' ]
            ],
            'plugins' => ['imagemanager' => \vova07\imperavi\bundles\ImageManagerAsset::class,]
        ]); ?>

        <?= $form->field($news, 'title_ru')->textInput(['max' => 255, 'required' => true]); ?>

        <?= $form->field($news, 'content_ru')->widget(\vova07\imperavi\Widget::class, [
            'settings' => [
                'imageUpload' => '/news/image-upload',
                'imageDelete' => '/news/file-delete',
                'imageManagerJson' => '/news/images-get',
                'plugins' => [ 'fullscreen' ]
            ],
            'plugins' => ['imagemanager' => \vova07\imperavi\bundles\ImageManagerAsset::class,]
        ]); ?>

        <div class="form-group text-center">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Cancel', '/news', ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>