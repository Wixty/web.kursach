<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FAS;
use kartik\grid\{ GridView, ActionColumn };

/* @var $this \yii\web\View */
/* @var $searchModel \backend\models\NewsSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */


$this->title = 'All News';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="pull-right">
    <a href="<?= Url::to(['/news/edit']); ?>" class="btn btn-primary">Add News</a>
</div>

<h1><?= $this->title ?></h1>

<div class="row">
    <div class="col-lg-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{summary}\n{pager}\n{items}{pager}",
            'export' => false,
            'pager' => [
                'maxButtonCount' => 18,
                'prevPageLabel' => FAS::icon('chevron-left'),
                'nextPageLabel' => FAS::icon('chevron-right'),
                'firstPageLabel' => FAS::icon('chevron-left').FAS::icon('chevron-left'),
                'lastPageLabel' => FAS::icon('chevron-right').FAS::icon('chevron-right'),
            ],
            'columns' => [
                'id',
                'title_en',
                'title_ru',
                [
                    'class' => ActionColumn::class,
                    'template' => '{edit} {delete}',
                    'buttons' => [
                        'edit' => function ($url, $model) {
                            return Html::a(FAS::icon('edit'),
                                Url::to(['/news/edit', 'id' => $model->id]),
                                ['type'=> 'button', 'title'=> 'Edit news', 'class'=>'btn btn-info']);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a(FAS::icon('trash-alt'),
                                Url::to(['/news/delete', 'id' => $model->id]),
                                ['type'=>'button', 'title'=> 'Delete news', 'class'=> 'btn btn-danger',
                                    'data-method' => 'post', 'data-confirm' => 'Are you sure?']);
                        },
                    ],
                ],
            ]
        ]) ?>
    </div>
</div>