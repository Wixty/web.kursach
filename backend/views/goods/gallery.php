<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use zxbodya\yii2\galleryManager\GalleryManager;

/* @var $this \yii\web\View */
/* @var $goods \common\models\Goods  */


$this->title = 'Goods Gallery';

$this->params['breadcrumbs'][] = ['label' => 'All Goods', 'url' => '/goods'];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= "{$this->title} for '{$goods->name_en}'" ?></h1>

<?= GalleryManager::widget(
    [
        'model' => $goods,
        'behaviorName' => 'gallery',
        'apiRoute' => '/goods/gallery-api',
    ]
); ?>

<style>
    .gallery-manager .btn-toolbar .btn-group label {
        padding-left: 26px;
    }
    .gallery-manager .btn-toolbar .btn-group label input {
        margin-left: -16px;
        position: absolute;
    }
    .gallery-manager .progress-overlay .modal {
        top: 20px;
    }
    .gallery-manager .image-preview {
        overflow: unset;
    }
    .gallery-manager .photo img {
        max-width: 130px;
        max-height: 140px;
        height: unset;
        width: unset;
    }
</style>
