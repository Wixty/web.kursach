<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use kartik\grid\{ GridView, ActionColumn };
use rmrevin\yii\fontawesome\FAS;
use common\models\Goods;

/* @var $this \yii\web\View */
/* @var $searchModel \backend\models\GoodsSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $category_list array */
/* @var $type_list array */


$this->title = 'All Goods';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pull-right">
    <a href="<?= Url::to(['/goods/edit']) ?>" class="btn btn-primary">Add Goods</a>
</div>

<h1><?= $this->title ?></h1>

<div class="row">
    <div class="col-lg-12">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{summary}\n{pager}\n{items}\n{pager}",
            'export' => false,
            'pager' => [
                'maxButtonCount' => 18,
                'prevPageLabel' => FAS::icon('chevron-left'),
                'nextPageLabel' => FAS::icon('chevron-right'),
                'firstPageLabel' => FAS::icon('chevron-left').FAS::icon('chevron-left'),
                'lastPageLabel' => FAS::icon('chevron-right').FAS::icon('chevron-right'),
            ],
            'columns' => [
                [
                    'attribute' => 'id',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'type_id',
                    'label' => 'Type',
                    'value' => function ($goods) use ($type_list) {
                        /* @var $goods Goods */
                        return $type_list[$goods->category->type_id];
                    },
                    'filter' => $type_list,
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'category_id',
                    'label' => 'Category',
                    'value' => function ($goods) use ($category_list) {
                        /* @var $goods Goods */
                        return $category_list[$goods->category_id];
                    },
                    'filter' => $category_list,
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'image',
                    'format' => 'html',
                    'value' => function ($model) {
                        /* @var $model Goods */
                        return Html::img($model->imageFullUrl, ['class' => 'goods-img']);
                    },
                    'width' => '100px',
                ],
                [
                    'attribute' => 'name_en',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'price',
                    'width' => '100px',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'in_stock',
                    'width' => '100px',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{edit} {gallery}<br>{delete}',
                    'buttons' => [
                        'edit' => function ($url, $model) {
                            return Html::a(FAS::icon('edit'),
                                Url::to(['/goods/edit', 'id' => $model->id]),
                                ['type'=> 'button', 'title'=> 'Edit goods', 'class'=>'btn btn-info bottom-6px']);
                        },
                        'gallery' => function ($url, $model) {
                            return Html::a(FAS::icon('images'),
                                Url::to(['/goods/gallery', 'id' => $model->id]),
                                ['type'=>'button', 'title'=> 'Gallery', 'class'=> 'btn btn-info bottom-6px']);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a(FAS::icon('trash-alt'),
                                Url::to(['/goods/delete', 'id' => $model->id]),
                                ['type'=>'button', 'title'=> 'Delete goods', 'class'=> 'btn btn-danger',
                                    'data-method' => 'post', 'data-confirm' => 'Are you sure?']);
                        },
                    ],
                ],
            ]
        ]) ?>

    </div>
</div>