<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;

/* @var $this \yii\web\View */
/* @var $goods \common\models\Goods  */
/* @var $dropdown_list array */


$this->title = $goods->id > 0 ? 'Edit Goods' : 'Add Goods';

$this->params['breadcrumbs'][] = ['label' => 'All Goods', 'url' => '/goods'];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= $goods->id > 0 ? "Edit '$goods->name_en'" : 'Add Goods' ?></h1>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-6">
        <?= $form->field($goods, 'name_en')->textInput(['max' => 255, 'required' => true]); ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($goods, 'name_ru')->textInput(['max' => 255, 'required' => true]); ?>
    </div>

    <div class="col-lg-12">
        <?= $form->field($goods, 'description_en')->widget(\vova07\imperavi\Widget::class, [
            'settings' => [
                'imageUpload' => '/goods/image-upload',
                'imageDelete' => '/goods/file-delete',
                'imageManagerJson' => '/goods/images-get',
                'plugins' => [ 'fullscreen' ]
            ],
            'plugins' => ['imagemanager' => \vova07\imperavi\bundles\ImageManagerAsset::class,]
        ]); ?>

        <?= $form->field($goods, 'description_ru')->widget(\vova07\imperavi\Widget::class, [
            'settings' => [
                'imageUpload' => '/goods/image-upload',
                'imageDelete' => '/goods/file-delete',
                'imageManagerJson' => '/goods/images-get',
                'plugins' => [ 'fullscreen' ]
            ],
            'plugins' => ['imagemanager' => \vova07\imperavi\bundles\ImageManagerAsset::class,]
        ]); ?>
    </div>

    <div class="col-lg-6">
        <?= $form->field($goods, 'category_id')->label('Category')
            ->dropDownList($dropdown_list, ['prompt' => 'Not set']) ?>

        <?= $form->field($goods, 'price')->widget(NumberControl::class, [
            'maskedInputOptions' => [ 'prefix' => '$ ', 'suffix' => ' c', ]
        ]) ?>

        <?= $form->field($goods, 'in_stock')->widget(NumberControl::class, [
            'maskedInputOptions' => [ 'digits' => 0, 'groupSeparator' => ' ', ],
        ]) ?>
    </div>

    <div class="col-lg-3">
        <?= $form->field($goods, 'image')->fileInput() ?>
    </div>

    <div class="col-lg-3">
        <?php if ($goods->image) : ?>
            <?= Html::img($goods->imageFullUrl, ['class' => 'goods-img']) ?>
        <?php endif; ?>
    </div>

    <div class="col-lg-12">
        <div class="form-group text-center">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Cancel', '/goods', ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>