<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use kartik\form\ActiveForm;

/* @var $this \yii\web\View */
/* @var $order \common\models\Order  */
/* @var $status_list array */
/* @var $pay_list array */
/* @var $carts \common\models\Cart[] */


$this->title = 'Show Order';

$this->params['breadcrumbs'][] = ['label' => 'All Orders', 'url' => '/orders'];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= "{$this->title} №{$order->id}" ?></h1>

<div class="row">
    <?php $form = ActiveForm::begin() ?>
    <div class="col-lg-3">
        <?= $form->field($order, 'id')->textInput(['readonly' => true]) ?>
        <?= $form->field($order, 'name')->textInput(['readonly' => true]) ?>
        <?= $form->field($order, 'address')->textInput(['readonly' => true]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($order, 'pay_method')->dropDownList($pay_list, ['readonly' => true]) ?>
        <?= $form->field($order, 'telephone')->textInput(['readonly' => true]) ?>
        <?= $form->field($order, 'email')->textInput(['readonly' => true]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($order, 'ip')->textInput(['readonly' => true]) ?>
        <?= $form->field($order, 'created')->textInput(['readonly' => true]) ?>
        <?= $form->field($order, 'updated')->textInput(['readonly' => true]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($order, 'comment')->textarea(['rows' => 2, 'readonly' => true]) ?>
        <?= $form->field($order, 'status')->dropDownList($status_list) ?>
        <div class="form-group text-center">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Cancel', '/orders', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center bold">Goods in this order</h3>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <td>Goods ID</td>
                    <td>Type</td>
                    <td>Category</td>
                    <td>Name</td>
                    <td>Photo</td>
                    <td>Price</td>
                    <td>Count</td>
                </tr>
                </thead>
                <tbody>
                <?php $total_price = $total_count = 0;
                foreach ($carts as $cart) :
                    echo $this->render('_goods', compact('cart'));
                    $total_price += $cart->goods->price * $cart->count;
                    $total_count += $cart->count;
                endforeach; ?>
                </tbody>
                <tfoot class="bold">
                <tr>
                    <td colspan="5" class="table-total">Total</td>
                    <td>$<?= $total_price ?></td>
                    <td><?= $total_count ?></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

</div>