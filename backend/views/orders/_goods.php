<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $cart \common\models\Cart */

$goods = $cart->goods;

?>
<tr>
    <td><?= $goods->id ?></td>
    <td><?= $goods->category->type->name ?></td>
    <td><?= $goods->category->name ?></td>
    <td>
        <a href="<?= Url::to(['/goods/edit', 'id' => $goods->id]) ?>">
            <?= $goods->name ?>
        </a>
    </td>
    <td class="img-parent">
        <?= Html::img($goods->imageFullUrl) ?>
    </td>
    <td>$<?= $goods->price ?></td>
    <td><?= $cart->count ?></td>
</tr>