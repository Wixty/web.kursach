<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use common\models\Cart;
use common\models\Order;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\grid\{ GridView, ActionColumn };
use rmrevin\yii\fontawesome\FAS;

/* @var $this \yii\web\View */
/* @var $searchModel \backend\models\OrderSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $status_list array */
/* @var $pay_list array */


$this->title = 'All Orders';

$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= $this->title ?></h1>

<div class="row">
    <div class="col-lg-12">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{summary}\n{pager}\n{items}\n{pager}",
            'export' => false,
            'pager' => [
                'maxButtonCount' => 18,
                'prevPageLabel' => FAS::icon('chevron-left'),
                'nextPageLabel' => FAS::icon('chevron-right'),
                'firstPageLabel' => FAS::icon('chevron-left').FAS::icon('chevron-left'),
                'lastPageLabel' => FAS::icon('chevron-right').FAS::icon('chevron-right'),
            ],
            'columns' => [
                [
                    'attribute' => 'id',
                    'width' => '6%',
                ],
                'name',
                'telephone',
                'email',
                [
                    'label' => 'Items',
                    'value' => function ($order) {
                        /* @var $order Order */
                        return array_sum(ArrayHelper::getColumn($order->carts, 'count'));
                    }
                ],
                [
                    'label' => 'Price',
                    'value' => function ($order) {
                        /* @var $order Order */
                        $prices = ArrayHelper::getColumn($order->carts, function ($cart) {
                            /* @var $cart Cart */
                            return $cart->count * $cart->goods->price;
                        });
                        return '$'.array_sum($prices);
                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'html',
                    'value' => function ($order) use ($status_list) {
                        switch ($order->status) {
                            case 0: return "<span class=\"label label-danger\">{$status_list[$order->status]}</span>";
                            case 1: return "<span class=\"label label-primary\">{$status_list[$order->status]}</span>";
                            default: return "<span class=\"label label-success\">{$status_list[$order->status]}</span>";
                        }

                    },
                    'filter' => $status_list
                ],
                [
                    'label' => 'Created and Updated',
                    'format' => 'html',
                    'value' => function ($order) {
                        return "<b>C</b>: $order->created<br><b>U</b>: $order->updated";
                    }
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{show}',
                    'buttons' => [
                        'show' => function ($url, $order) {
                            return Html::a(FAS::icon('edit'),
                                Url::to(['/orders/show', 'id' => $order->id]), [
                                    'type' => 'button', 'title' => 'Show order', 'class' => 'btn btn-info',
                                ]);
                        },
                    ],
                ],
            ]
        ]) ?>

    </div>
</div>