<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use kartik\grid\{ GridView, ActionColumn };
use rmrevin\yii\fontawesome\FAS;
use common\models\Category;

/* @var $this \yii\web\View */
/* @var $searchModel \backend\models\CategorySearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $type_list array */


$this->title = 'All Categories';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pull-right">
    <a href="<?= Url::to(['/category/edit']) ?>" class="btn btn-primary">Add Category</a>
</div>

<h1><?= $this->title ?></h1>

<div class="row">
    <div class="col-lg-12">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{summary}\n{pager}\n{items}\n{pager}",
            'export' => false,
            'pager' => [
                'maxButtonCount' => 18,
                'prevPageLabel' => FAS::icon('chevron-left'),
                'nextPageLabel' => FAS::icon('chevron-right'),
                'firstPageLabel' => FAS::icon('chevron-left').FAS::icon('chevron-left'),
                'lastPageLabel' => FAS::icon('chevron-right').FAS::icon('chevron-right'),
            ],
            'columns' => [
                [
                    'attribute' => 'id',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'width' => '8%',
                ],
                [
                    'attribute' => 'type_id',
                    'label' => 'Type',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'value' => function ($category) use ($type_list) {
                        /* @var $category Category */
                        return $type_list[$category->type_id];
                    },
                    'filter' => $type_list,
                ],
                [
                    'attribute' => 'name_en',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'name_ru',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{edit} {delete}',
                    'buttons' => [
                        'edit' => function ($url, $model) {
                            return Html::a(FAS::icon('edit'),
                                Url::to(['/category/edit', 'id' => $model->id]),
                                ['type' => 'button', 'title' => 'Edit category', 'class' => 'btn btn-info']);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a(FAS::icon('trash-alt'),
                                Url::to(['/category/delete', 'id' => $model->id]),
                                ['type' => 'button', 'title' => 'Delete category', 'class' => 'btn btn-danger',
                                    'data-method' => 'post', 'data-confirm' => 'Are you sure?']);
                        },
                    ],
                ],
            ]
        ]) ?>

    </div>
</div>