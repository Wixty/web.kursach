<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use kartik\form\ActiveForm;

/* @var $this \yii\web\View */
/* @var $category \common\models\Category  */
/* @var $type_list array */


$this->title = $category->id > 0 ? 'Edit Category' : 'Add Category';

$this->params['breadcrumbs'][] = ['label' => 'All Categories', 'url' => '/category'];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= $category->id > 0 ? "Edit '$category->name_en'" : $this->title ?></h1>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-12">

        <?= $form->field($category, 'name_en')->textInput(['max' => 255, 'required' => true]); ?>
        <?= $form->field($category, 'name_ru')->textInput(['max' => 255, 'required' => true]); ?>

        <?= $form->field($category, 'type_id')->label('Type')
            ->dropDownList($type_list, ['required' => true]) ?>

        <div class="form-group text-center">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Cancel', '/category', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>