<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use kartik\form\ActiveForm;

/* @var $this \yii\web\View */
/* @var $page \common\models\Page  */


$this->title = 'Edit Page';

$this->params['breadcrumbs'][] = ['label' => 'All Pages', 'url' => '/pages'];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= "{$this->title} '{$page->title_en}'" ?></h1>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-6">
        <?= $form->field($page, 'title_en')->textInput(['readonly' => true]); ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($page, 'url')->textInput(['readonly' => true]); ?>
    </div>
    <div class="col-lg-12">
        <?= $form->field($page, 'content_en')->widget(\vova07\imperavi\Widget::class, [
            'settings' => [
                'imageUpload' => '/pages/image-upload',
                'imageDelete' => '/pages/file-delete',
                'imageManagerJson' => '/pages/images-get',
                'plugins' => [ 'fullscreen' ]
            ],
            'plugins' => ['imagemanager' => \vova07\imperavi\bundles\ImageManagerAsset::class,]
        ]); ?>

        <?= $form->field($page, 'content_ru')->widget(\vova07\imperavi\Widget::class, [
            'settings' => [
                'imageUpload' => '/pages/image-upload',
                'imageDelete' => '/pages/file-delete',
                'imageManagerJson' => '/pages/images-get',
                'plugins' => [ 'fullscreen' ]
            ],
            'plugins' => ['imagemanager' => \vova07\imperavi\bundles\ImageManagerAsset::class,]
        ]); ?>

        <div class="form-group text-center">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Cancel', '/pages', ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>