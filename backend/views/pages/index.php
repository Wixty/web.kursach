<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use kartik\grid\{ GridView, ActionColumn };
use rmrevin\yii\fontawesome\FAS;

/* @var $this \yii\web\View */
/* @var $searchModel \backend\models\PageSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */


$this->title = 'All Pages';

$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= $this->title ?></h1>

<div class="row">
    <div class="col-lg-12">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{summary}\n{pager}\n{items}\n{pager}",
            'export' => false,
            'pager' => [
                'maxButtonCount' => 18,
                'prevPageLabel' => FAS::icon('chevron-left'),
                'nextPageLabel' => FAS::icon('chevron-right'),
                'firstPageLabel' => FAS::icon('chevron-left').FAS::icon('chevron-left'),
                'lastPageLabel' => FAS::icon('chevron-right').FAS::icon('chevron-right'),
            ],
            'columns' => [
                'id',
                'url',
                [
                    'attribute' => 'title_en',
                    'width' => '60%',
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{edit}',
                    'buttons' => [
                        'edit' => function ($url, $model) {
                            return Html::a(FAS::icon('edit'),
                                Url::to(['/pages/edit', 'id' => $model->id]), [
                                    'type'=> 'button', 'title'=> 'Edit page', 'class'=>'btn btn-info',
                                ]);
                        },
                    ],
                ],
            ]
        ]) ?>

    </div>
</div>