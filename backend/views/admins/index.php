<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use kartik\grid\{ GridView, ActionColumn };
use rmrevin\yii\fontawesome\FAS;
use backend\models\Admin;

/* @var $this \yii\web\View */
/* @var $searchModel \backend\models\AdminSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $identity Admin */


$this->title = 'All Admins';

$this->params['breadcrumbs'][] = $this->title;

$statuses = array_keys(Admin::$statuses);
$max_status = max($statuses);
$min_status = min($statuses);

?>
<?php if ($identity->canCreateAdmins) : ?>
    <div class="pull-right">
        <a href="<?= Url::to(['/admins/add']); ?>" class="btn btn-primary">Add Admin</a>
    </div>
<?php endif; ?>

<h1><?= $this->title ?></h1>

<div class="row">
    <div class="col-lg-12">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{summary}\n{pager}\n{items}\n{pager}",
            'export' => false,
            'pager' => [
                'maxButtonCount' => 18,
                'prevPageLabel' => FAS::icon('chevron-left'),
                'nextPageLabel' => FAS::icon('chevron-right'),
                'firstPageLabel' => FAS::icon('chevron-left').FAS::icon('chevron-left'),
                'lastPageLabel' => FAS::icon('chevron-right').FAS::icon('chevron-right'),
            ],
            'columns' => [
                'username',
                'email',
                [
                    'label' => 'Login Data',
                    'format' => 'raw',
                    'value' => function($model){
                        $created_at = $model->created_at ? date('d.m.Y H:i', $model->created_at) : '-----';
                        $last_login = $model->last_login ? date('d.m.Y H:i', $model->last_login) : '-----';
                        $updated_at  = $model->updated_at  ? date('d.m.Y H:i', $model->updated_at ) : '-----';
                        return  "<div>Created: {$created_at}</div><div>Updated: {$updated_at}</div><div>Logined: {$last_login}</div>";
                    },
                ],
                'ip',
                [
                    'attribute' => 'status',
                    'content' => function($model) {
                        if (\in_array($model->status, Admin::STATUS_ACTIVE)) $color = 'green';
                        else $color = 'red';
                        return '<span style="color:'.$color.'">'.(Admin::$statuses[$model->status] ?? 'unknown').'</span>';
                    },
                    'filter' => Admin::$statuses,
                    'width' => '12%',
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{edit}<br>{status-up} {status-down}',
                    'buttons' => [
                        'edit' => function ($url, $model) use ($identity) {
                            // если залогиненому админу разрешено редактирование других ИЛИ выбран свой профиль
                            if ($identity->canEditAdmins || $identity->id === $model->id) {
                                return Html::a(FAS::icon('edit'),
                                    Url::to(['/admins/edit', 'id' => $model->id]), [
                                        'type' => 'button', 'title' => 'Change login, email, password',
                                        'class' => 'btn btn-info bottom-6px',
                                    ]);
                            }
                            return '';
                        },
                        'status-up' => function ($url, $model) use ($identity, $max_status) {
                            if ($identity->canEditStatus          // если может изменять статусы
                                && $identity->id !== $model->id   // И выбран не свой аккаунт
                                && $model->status < $max_status) {// И статус выбранного админа меньше max

                                return Html::a(FAS::icon('plus'),
                                    Url::to(['/admins/change-status', 'id' => $model->id, 'direction' => '+']), [
                                        'type' => 'button', 'title' => 'Status UP',
                                        'class' => 'btn btn-success', 'data-method' => 'post',
                                    ]);
                            }
                            return '';
                        },
                        'status-down' => function ($url, $model) use ($identity, $min_status) {
                            if ($identity->canEditStatus           // если может изменять статусы
                                && $identity->id !== $model->id    // И выбран не свой аккаунт
                                && $model->status > $min_status) { // И статус выбранного админа больше min

                                $disabled = $model->status === Admin::STATUS_DISABLED;
                                return Html::a(FAS::icon($disabled ? 'trash' : 'minus'),
                                    Url::to(['/admins/change-status', 'id'=>$model->id, 'direction' => '-']), [
                                        'type' => 'button', 'title' => $disabled ? 'Delete' : 'Status DOWN',
                                        'class' => 'btn btn-danger', 'data-method' => 'post',
                                    ]);
                            }
                            return '';
                        },
                    ],
                ],
            ]
        ]) ?>

    </div>
</div>