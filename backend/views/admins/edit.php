<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use kartik\form\ActiveForm;

/* @var $this \yii\web\View */
/* @var $admin \backend\models\Admin  */


$this->title = 'Edit Admin';

$this->params['breadcrumbs'][] = ['label' => 'All Admins', 'url' => '/admins'];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= "{$this->title} {$admin->username}" ?></h1>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-6">
        <?= $form->field($admin, 'username') ?>
        <?= $form->field($admin, 'email') ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Cancel', '/admins', ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <?= Html::label('New password', 'psw', ['class' => 'control-label']) ?>
            <?= Html::passwordInput('psw', null, ['class' => 'form-control', 'id' => 'psw', 'minlength' => 6]) ?>
        </div>
        <div class="form-group">
            <?= Html::label('New password (confirmation)', 'chk', ['class' => 'control-label']) ?>
            <?= Html::passwordInput('chk', null, ['class' => 'form-control', 'id' => 'chk', 'minlength' => 6]) ?>
        </div>
        <span class="alert alert-info" style="line-height: 3em;">
            Password fields is optional.
        </span>
    </div>

    <?php ActiveForm::end(); ?>
</div>