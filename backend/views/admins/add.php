<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\helpers\Url;
use yii\bootstrap\Html;
use kartik\form\ActiveForm;
use backend\models\Admin;

/* @var $this \yii\web\View */
/* @var $admin \backend\models\Admin */


$this->title = 'Create Admin';

$this->params['breadcrumbs'][] = ['label' => 'All Admins', 'url' => '/admins'];
$this->params['breadcrumbs'][] = $this->title;

$status_new = Admin::$statuses;
unset($status_new[Admin::STATUS_DELETED])

?>
<h1><?= $this->title ?></h1>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-6">
        <?= $form->field($admin, 'username')->textInput(['required' => true]) ?>
        <?= $form->field($admin, 'email')->textInput(['type' => 'email', 'required' => true]) ?>
        <?= $form->field($admin, 'status')->dropDownList($status_new, ['required' => true]) ?>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <?= Html::label('Password', 'psw', ['class' => 'control-label']) ?>
            <?= Html::passwordInput('psw', null, ['id' => 'psw', 'class' => 'form-control', 'minlength' => 6, 'required' => '']) ?>
        </div>
        <div class="form-group">
            <?= Html::label('Confirmation', 'chk', ['class' => 'control-label']) ?>
            <?= Html::passwordInput('chk', null, ['id' => 'chk', 'class' => 'form-control', 'minlength' => 6, 'required' => '']) ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
            <a href="<?= Url::to(['/admins']); ?>" class="btn btn-default"><?= 'Cancel' ?></a>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>