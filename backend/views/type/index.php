<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use kartik\grid\{ GridView, ActionColumn };
use rmrevin\yii\fontawesome\FAS;

/* @var $this \yii\web\View */
/* @var $searchModel \backend\models\TypeSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */


$this->title = 'All Types';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pull-right">
    <a href="<?= Url::to(['/type/edit']) ?>" class="btn btn-primary">Add Type</a>
</div>

<h1><?= $this->title ?></h1>

<div class="row">
    <div class="col-lg-12">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{summary}\n{pager}\n{items}\n{pager}",
            'export' => false,
            'pager' => [
                'maxButtonCount' => 18,
                'prevPageLabel' => FAS::icon('chevron-left'),
                'nextPageLabel' => FAS::icon('chevron-right'),
                'firstPageLabel' => FAS::icon('chevron-left').FAS::icon('chevron-left'),
                'lastPageLabel' => FAS::icon('chevron-right').FAS::icon('chevron-right'),
            ],
            'columns' => [
                [
                    'attribute' => 'id',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'width' => '10%',
                ],
                [
                    'attribute' => 'name_en',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'name_ru',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{edit} {delete}',
                    'buttons' => [
                        'edit' => function ($url, $model) {
                            return Html::a(FAS::icon('edit'),
                                Url::to(['/type/edit', 'id' => $model->id]),
                                ['type'=> 'button', 'title'=> 'Edit type', 'class'=>'btn btn-info']);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a(FAS::icon('trash-alt'),
                                Url::to(['/type/delete', 'id' => $model->id]),
                                ['type'=>'button', 'title'=> 'Delete type', 'class'=> 'btn btn-danger', 'data-method' => 'post',
                                    'data-confirm' => 'Are you sure? All categories for this type wil be deleted too!']);
                        },
                    ],
                ],
            ]
        ]) ?>

    </div>
</div>