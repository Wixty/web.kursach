<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

use yii\bootstrap\Html;
use kartik\form\ActiveForm;

/* @var $this \yii\web\View */
/* @var $type \common\models\Type  */


$this->title = $type->id > 0 ? 'Edit Type' : 'Add Type';

$this->params['breadcrumbs'][] = ['label' => 'All Types', 'url' => '/type'];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= $type->id > 0 ? "Edit '$type->name_en'" : $this->title ?></h1>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-12">

        <?= $form->field($type, 'name_en')->textInput(['max' => 255, 'required' => true]); ?>
        <?= $form->field($type, 'name_ru')->textInput(['max' => 255, 'required' => true]); ?>

        <div class="form-group text-center">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Cancel', '/type', ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>
</div>