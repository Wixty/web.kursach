<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\{ Nav, NavBar };
use yii\widgets\Breadcrumbs;
use rmrevin\yii\fontawesome\FAS;
use common\widgets\Alert;
use backend\assets\AppAsset;
use backend\models\Admin;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php $leftItems = $rightItems = [];

    if (Yii::$app->user->isGuest) {

        $rightItems[] = ['label' => FAS::icon('sign-in-alt').'Login', 'url' => ['/site/login']];

    } else { // !guest

        /** @var $identity Admin Залогиненый админ */
        $identity = Yii::$app->user->identity;

        if ($identity->canCreateAdmins) {
            $rightItems[] = [
                'label' => 'Admins',
                'items' => [
                    ['label' => 'Show all', 'url' => '/admins'],
                    ['label' => 'Add new', 'url' => '/admins/add'],
                ]
            ];
        } else {
            $rightItems[] = ['label' => 'Admins', 'url' => '/admins'];
        }
        if ($identity->canEditNews) {
            $leftItems[] = [
                'label' => 'News',
                'items' => [
                    ['label' => 'Show all', 'url' => '/news'],
                    ['label' => 'Add new', 'url' => '/news/edit'],
                ]
            ];
        }
        if ($identity->canEditPages) {
            $leftItems[] = ['label' => 'Pages', 'url' => '/pages'];
        }
        if ($identity->canEditGoods) {
            $leftItems[] = [
                'label' => 'Types',
                'items' => [
                    ['label' => 'Show all', 'url' => '/type'],
                    ['label' => 'Add new', 'url' => '/type/edit'],
                ]
            ];
            $leftItems[] = [
                'label' => 'Categories',
                'items' => [
                    ['label' => 'Show all', 'url' => '/category'],
                    ['label' => 'Add new', 'url' => '/category/edit'],
                ]
            ];
            $leftItems[] = [
                'label' => 'Goods',
                'items' => [
                    ['label' => 'Show all', 'url' => '/goods'],
                    ['label' => 'Add new', 'url' => '/goods/edit'],
                ]
            ];
        }
        if ($identity->canEditOrders) {
            $leftItems[] = ['label' => 'Orders', 'url' => '/orders'];
        }

        $rightItems[] = Html::tag('li', Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(FAS::icon('sign-out-alt').Yii::$app->user->identity->username,
                ['class' => 'btn btn-link logout'])
            . Html::endForm());
    }
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $leftItems,
        'encodeLabels' => false,
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $rightItems,
        'encodeLabels' => false,
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => $this->params['breadcrumbs'] ?? [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<?php
$host = Yii::$app->urlManager->hostInfo;
$url = preg_replace('/\/\/a\.s\./', '//s.', $host, 1, $count);
if ($count < 1) $url = preg_replace('/\/\/admin\.shop\./', '//shop.', $host, 1);
?>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
        <p class="text-center"><?= Html::a('Main Site', $url, ['style' => 'margin:0 20px']) ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
