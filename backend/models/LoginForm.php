<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\models;

use Yii;
use yii\base\Model;


class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_admin = false;

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params): void
    {
        if (!$this->hasErrors()) {
            $admin = $this->getAdmin();
            if (!$admin || !$admin->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in the admin using the provided username and password.
     * @return bool whether the admin is logged in successfully
     */
    public function login(): bool
    {
        if ($this->validate()) {
            $admin = $this->getAdmin();
            if ($admin) {
                if ($admin->hasAttribute('last_login'))
                    $admin->updateAttributes(['last_login' => time()]);
                return Yii::$app->user->login($admin, $this->rememberMe ? 2592000 : 0); // 30 days
            }
        }
        return false;
    }

    /**
     * Finds admin by [[username]]
     * @return Admin|null
     */
    protected function getAdmin():? Admin
    {
        if ($this->_admin === false) {
            $this->_admin = Admin::findByUsername($this->username);
        }
        return $this->_admin;
    }
}
