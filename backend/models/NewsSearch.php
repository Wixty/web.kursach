<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\models;

use common\models\News;
use yii\data\ActiveDataProvider;


class NewsSearch extends News
{

    public function rules()
    {
        return [
            [['title_en', 'title_ru'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title_en', $this->title_en]);
        $query->andFilterWhere(['like', 'title_ru', $this->title_ru]);

        return $dataProvider;
    }

}