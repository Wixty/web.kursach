<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\models;

use yii\data\ActiveDataProvider;


class AdminSearch extends Admin
{

    public function rules()
    {
        return [
            [['username', 'email', 'ip'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = Admin::find() // скроем удалённых
            ->where(['not', ['status' => Admin::STATUS_DELETED]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'ip', $this->ip]);

        return $dataProvider;
    }

}