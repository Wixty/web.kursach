<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\Order;


class OrderSearch extends Order
{

    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['name', 'telephone', 'email'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = parent::find()->with('carts', 'carts.goods');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['id' => $this->id, 'status' => $this->status]);

        return $dataProvider;
    }

}