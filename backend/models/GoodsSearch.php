<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\{ Category, Goods };


class GoodsSearch extends Goods
{

    public $type_id;

    public function rules()
    {
        return [
            [['name_en', 'name_ru'], 'string'],
            [['price'], 'double', 'min' => 0.01],
            [['in_stock'], 'integer', 'min' => 0],
            [['type_id', 'category_id'], 'integer', 'min' => 1],
        ];
    }

    public function search($params)
    {
        $query = parent::find()->with('category');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $goodz = Goods::tableName();
        $catz = Category::tableName();

        $query->andFilterWhere(['category_id' => $this->category_id])
            ->andFilterWhere(['price' => $this->price])
            ->andFilterWhere(['like', "$goodz.`name_en`", $this->name_en])
            ->andFilterWhere(['like', "$goodz.`name_ru`", $this->name_ru]);

        if (!empty($this->type_id)) {
            $query->innerJoinWith('category')
                ->andFilterWhere(['type_id' => $this->type_id])
                ->select(["$goodz.*", "$catz.`type_id`"]);
        }

        return $dataProvider;
    }

}