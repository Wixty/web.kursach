<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\models;

use common\models\Page;
use yii\data\ActiveDataProvider;


class PageSearch extends Page
{

    public function rules()
    {
        return [
            [['title_en', 'url'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = parent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title_en', $this->title_en]);
        $query->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }

}