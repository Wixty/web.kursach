<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\Type;


class TypeSearch extends Type
{

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name_en', 'name_ru'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = parent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['like', 'name_en', $this->title_en])
            ->andFilterWhere(['like', 'name_ru', $this->title_ru]);

        return $dataProvider;
    }

}