<?php
/**
 * Copyright (c) 2018
 * Developed by Wixty (wixtys@gmail.com)
 * All rights reserved
 */

namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


/**
 * Admin model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $last_login
 * @property string $ip
 *
 * @property-write string $password write-only password
 *
 * @property-read bool $canCreateAdmins
 * @property-read bool $canEditAdmins
 * @property-read bool $canEditStatus
 * @property-read bool $canEditPages
 * @property-read bool $canEditNews
 * @property-read bool $canEditGoods
 * @property-read bool $canEditOrders
 */
class Admin extends ActiveRecord implements IdentityInterface
{

    public const STATUS_DELETED = 0;
    public const STATUS_DISABLED = 2;
    public const STATUS_MODERATOR = 8;
    public const STATUS_SUPER_ADMIN = 10;

    /**
     * @var array Массив со значениями ВСЕХ АКТИВНЫХ статусов
     */
    public const STATUS_ACTIVE = [
        self::STATUS_MODERATOR,
        self::STATUS_SUPER_ADMIN
    ];

    /**
     * @var array Массив со всеми возможными статусами: [ число-статус => строка-название ].
     * ДОЛЖЕН содержать все возможные варианты статусов. Ключи в порядке ВОЗРАСТАНИЯ.
     */
    public static $statuses = [
        self::STATUS_DELETED => 'Deleted',
        self::STATUS_DISABLED => 'Disabled',
        self::STATUS_MODERATOR => 'Moderator',
        self::STATUS_SUPER_ADMIN => 'SuperAdmin',
    ];

    public static function tableName()
    {
        return '{{%admin}}';
    }

    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['status'], 'safe'],
            [['status'], 'in', 'range' => array_keys(self::$statuses)],
            [['username', 'email'], 'string'],
            [['username'], 'unique'],
            [['username', 'email', 'status'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Login',
            'email' => 'Email',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
            'last_login' => 'Last login',
            'ip' => 'Last login IP',
            'status' => 'Status',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }


    public function getCanCreateAdmins() : bool
    {
        return $this->status === self::STATUS_SUPER_ADMIN;
    }

    public function getCanEditAdmins() : bool
    {
        return $this->status === self::STATUS_SUPER_ADMIN;
    }

    public function getCanEditStatus() : bool
    {
        return $this->status === self::STATUS_SUPER_ADMIN;
    }

    public function getCanEditPages() : bool
    {
        return \in_array($this->status, [self::STATUS_SUPER_ADMIN, self::STATUS_MODERATOR]);
    }

    public function getCanEditNews() : bool
    {
        return \in_array($this->status, [self::STATUS_SUPER_ADMIN, self::STATUS_MODERATOR]);
    }

    public function getCanEditGoods() : bool
    {
        return \in_array($this->status, [self::STATUS_SUPER_ADMIN, self::STATUS_MODERATOR]);
    }

    public function getCanEditOrders() : bool
    {
        return \in_array($this->status, [self::STATUS_SUPER_ADMIN, self::STATUS_MODERATOR]);
    }

}
